package com.android.wanetcaptin.holders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.wanetcaptin.R;
import com.android.wanetcaptin.models.AppFunctions;
import com.android.wanetcaptin.models.AppConstants;
import com.android.wanetcaptin.models.OrderLocation;
import com.android.wanetcaptin.models.Orders;
import com.android.wanetcaptin.models.User;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;


/**
 * Created by Samer AlShurafa on 2/9/2018.
 */


public class OrdersViewHolder extends RecyclerView.ViewHolder {


    private Context context;
    private CircularImageView cardUserImage;
    private TextView userName, orderDate, sourceName, destinationName;


    public OrdersViewHolder(View itemView) {
        super(itemView);

        this.context = itemView.getContext();

        cardUserImage = itemView.findViewById(R.id.cardUserImage);
        userName = itemView.findViewById(R.id.userName);
        orderDate = itemView.findViewById(R.id.orderDate);
        sourceName = itemView.findViewById(R.id.sourceName);
        destinationName = itemView.findViewById(R.id.destinationName);
    }


    public void updateUI(Orders orders) {
        User user = orders.getUserModel();

        userName.setText(user.getName());
        orderDate.setText(AppFunctions.getTimeDate(orders.getService_time()));

        OrderLocation orderLocation = orders.getSourceLocation();
        sourceName.setText(orderLocation.getName());

        orderLocation = orders.getDestinationLocation();
        destinationName.setText(orderLocation.getName());


        if(!user.getAvatar().isEmpty() && !user.getAvatar().equalsIgnoreCase(AppConstants.DB_AVATAR_CHILD_DEFAULT)) {

            Picasso.with(context)
                    .load(user.getAvatar())
                    .placeholder(R.drawable.profile_user)
                    .error(R.drawable.logo_orange)
                    .into(cardUserImage);

        }

    }

}



