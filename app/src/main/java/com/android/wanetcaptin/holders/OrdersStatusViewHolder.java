package com.android.wanetcaptin.holders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.wanetcaptin.R;
import com.android.wanetcaptin.models.Status;
import com.android.wanetcaptin.models.TimeLineStatus;


/**
 * Created by Samer AlShurafa on 2/16/2018.
 */


public class OrdersStatusViewHolder extends RecyclerView.ViewHolder {


    private ImageView statusImage;
    private TextView statusTitle, statusDescription;
    private View lineView;
    private LinearLayout statusLayout;

    private Context context;

    public OrdersStatusViewHolder(View itemView) {
        super(itemView);

        context = itemView.getContext();

        statusImage = itemView.findViewById(R.id.statusImage);
        statusTitle = itemView.findViewById(R.id.statusTitle);
        statusDescription = itemView.findViewById(R.id.statusDescription);
        lineView = itemView.findViewById(R.id.lineView);
        statusLayout = itemView.findViewById(R.id.statusLayout);
    }


    public void updateUI(TimeLineStatus status){
        if(status == null)
            return;

        statusTitle.setText(status.getStatusTitle());
        statusDescription.setText(status.getStatusDescription());

        if(status.getStatus() == Status.ON) {
            statusImage.setImageResource(R.mipmap.status_on);
            statusTitle.setTextColor(context.getResources().getColor(R.color.color_white));
            statusDescription.setTextColor(context.getResources().getColor(R.color.time_line_description_done));
            lineView.setBackgroundColor(context.getResources().getColor(R.color.color_magor_purple));
            statusLayout.setBackground(context.getResources().getDrawable(R.drawable.shape_time_line_layout_on));
        } else {
            statusImage.setImageResource(R.mipmap.status_off);
            statusTitle.setTextColor(context.getResources().getColor(R.color.time_line_title_not_done));
            statusDescription.setTextColor(context.getResources().getColor(R.color.time_line_description_not_done));
            lineView.setBackgroundColor(context.getResources().getColor(R.color.time_line_line_not_done));
            statusLayout.setBackground(context.getResources().getDrawable(R.drawable.shape_time_line_layout_off));
        }

        if(status.getIfLast()) {
            lineView.setVisibility(View.INVISIBLE);
        }

    }

}
