package com.android.wanetcaptin.adapters;

import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.wanetcaptin.R;

/**
 * Created by Samer AlShurafa on 1/20/2018.
 */


public class SliderAdapter extends PagerAdapter {


    private int[] slide_images = {R.drawable.intro_1, R.drawable.intro_2, R.drawable.intro_3};
    private int[] slide_headers = {R.string.header_first_page, R.string.header_second_page, R.string.header_third_page};
    private int[] slide_descriptions = {R.string.description_first_page, R.string.description_second_page, R.string.description_third_page};


    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.slide_layout, container, false);

        ImageView image = view.findViewById(R.id.introImage);
        TextView header = view.findViewById(R.id.introHeader);
        TextView description = view.findViewById(R.id.introDescription);

        image.setImageDrawable(container.getContext().getResources().getDrawable(slide_images[position]));
        header.setText(container.getContext().getResources().getString(slide_headers[position]));
        description.setText(container.getContext().getResources().getString(slide_descriptions[position]));

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ConstraintLayout)object);
    }

}
