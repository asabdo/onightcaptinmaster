package com.android.wanetcaptin.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import com.android.wanetcaptin.R;
import com.android.wanetcaptin.holders.OrdersStatusViewHolder;
import com.android.wanetcaptin.models.TimeLineStatus;

/**
 * Created by Samer AlShurafa on 2/16/2018.
 */


public class OrdersStatusAdapter extends RecyclerView.Adapter<OrdersStatusViewHolder> {


    private ArrayList<TimeLineStatus> arrayList;

    public OrdersStatusAdapter(ArrayList<TimeLineStatus> arrayList) {
        this.arrayList = arrayList;
    }


    @Override
    public OrdersStatusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_order_status_layout, parent, false);

        return new OrdersStatusViewHolder(view);
    }


    @Override
    public void onBindViewHolder(OrdersStatusViewHolder ordersStatusViewHolder, int position) {
        TimeLineStatus status = arrayList.get(position);
        ordersStatusViewHolder.updateUI(status);
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}


