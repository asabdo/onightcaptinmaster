package com.android.wanetcaptin.models;


/**
 * Created by Samer AlShurafa on 1/20/2018.
 */


public class AppConstants {


    public final static String FILE_NAME = "MyDate";

    public final static String FIRST_TIME_KEY = "first_time";
    public final static String SIGN_IN_KEY = "sign_in";
    public final static String DEFAULT_VALUE = "N/A";

    public final static int PROFILE_PICK_IMAGE_REQUEST = 71;

    public final static String DB_USERS_CHILD = "users";
    public final static String DB_AVATAR_CHILD = "avatar";
    public final static String DB_EMAIL_CHILD = "email";
    public final static String DB_ID_CHILD = "id";
    public final static String DB_MOBILE_NO_CHILD = "mobile_no";
    public final static String DB_NAME_CHILD = "name";
    public final static String DB_CAR_MODEL_CHILD = "car_model";
    public final static String DB_CAR_NUMBER_CHILD = "car_no";

    public final static String DB_AVATAR_CHILD_DEFAULT = "avatar.png";

    public final static String DB_EMPLOYEES_CHILD = "employees";
    public final static String DB_EMPLOYEES_LOCATION_CHILD = "location";

    public final static String DB_PRE_ORDERS_CHILD = "pre_orders";
    public final static String DB_PRE_ORDERS_EMP_STATUS_CHILD = "emp_status";
    public final static String DB_ORDERS_CHILD = "orders";
    public final static String DB_ORDERS_USER_STATUS_CHILD = "user_status";
    public final static String DB_ORDERS_DRIVER_STATUS_CHILD = "emp_status";
    public final static String DB_ORDERS_USER_MODEL_ID_CHILD = "userModel/id";
    public final static String DB_ORDERS_DRIVER_MODEL_ID_CHILD = "driverModel/id";


    public final static String DB_STATUS_CHILD = "status";
    public final static String DB_STATUS_CHILD_AVAILABLE = "available"; // employees
    public final static String DB_STATUS_CHILD_UNAVAILABLE = "unavailable"; // employees

    public final static String DB_STATUS_CHILD_ACCEPTED = "accepted"; // employees
    public final static String DB_STATUS_CHILD_REACHED = "reached"; // orders
    public final static String DB_STATUS_CHILD_IN_PROGRESS = "in_progress"; // order
    public final static String DB_STATUS_CHILD_CANCELLED = "cancelled"; // orders
    public final static String DB_STATUS_CHILD_COMPLETED = "completed"; // orders

    public final static String DB_ORDER_ID = "order_id"; // Shared ref
    public final static String DB_ORDER_PENDING = "pending"; // Shared ref
    public final static String DB_ORDER_IN_PROGRESS = "inProgress"; // query, = false or true.

    public final static String ORDERS_TO_DETAILS = "order_to_details"; // from orders to details or from dialog
    public final static String DETAILS_TO_TIME_LINE = "details_to_time_line";  // from details to time line
    public final static String TIME_LINE_TO_DETAILS = "time_line_to_details";  // from time line to details


    public final static int LOCATION_PERMISSION_CODE = 10;


    public final static String STORAGE_UPLOAD_USER_IMAGES_FOLDER_NAME = "users/";
    public final static String STORAGE_UPLOAD_EMPLOYEE_IMAGES_FOLDER_NAME = "employees/";
    public final static String STORAGE_IMAGE_EXTENSION_TYPE = ".jpg";


    public final static String FETCH_ADDRESS_INTENT_ACTION = "fetch_address";
    public final static String FETCH_ADDRESS_RESULT = "address_result";
    public final static String FETCH_ADDRESS_COUNTRY = "address_country";
    public final static String FETCH_ADDRESS_COUNTRY_SAUDI = "السعودية";

}




