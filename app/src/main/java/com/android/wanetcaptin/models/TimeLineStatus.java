package com.android.wanetcaptin.models;

/**
 * Created by Samer AlShurafa on 2/17/2018.
 */


public class TimeLineStatus {

    private String statusTitle;
    private String statusDescription;
    private Status status;
    private Boolean ifLast;

    public TimeLineStatus(String statusTitle, String statusDescription, Status status) {
        this.statusTitle = statusTitle;
        this.statusDescription = statusDescription;
        this.status = status;
        setIfLast(false);
    }


    public String getStatusTitle() {
        return statusTitle;
    }

    public String getStatusDescription() {
        return statusDescription;
    }


    public void setStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() { return status; }


    public void setIfLast(Boolean ifLast) {
        this.ifLast = ifLast;
    }

    public Boolean getIfLast() {
        return ifLast;
    }



}


