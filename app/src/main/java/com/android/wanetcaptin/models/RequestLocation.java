package com.android.wanetcaptin.models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Samer AlShurafa on 2/6/2018.
 */


public class RequestLocation {

    private LatLng latLng;
    private String locationName;

    RequestLocation(LatLng latLng, String locationName) {
        this.latLng = latLng;
        this.locationName = locationName;
    }


    public LatLng getLatLng() {
        return latLng;
    }

    public String getLocationName() {
        return locationName;
    }
}
