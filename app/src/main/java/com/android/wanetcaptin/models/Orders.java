package com.android.wanetcaptin.models;


/**
 * Created by Samer AlShurafa on 2/7/2018.
 */


public class Orders {

    private String id;
    private String service_type;
    private String status;
    private OrderLocation destinationLocation;
    private OrderLocation sourceLocation;
    private Employee driverModel;
    private Long service_time;
    private User userModel;


    public Orders() { }

    public Orders(String id) { // for pass it to current details fragment ..
        this.id = id;
    }

    public Orders(String id, String service_type, String status,
                  double destinationLatitude, double destinationLongitude, String destinationLocationName,
                  double sourceLatitude, double sourceLongitude, String sourceLocationName) {
        this.id = id;
        this.service_type = service_type;
        this.status = status;
        this.destinationLocation = new OrderLocation(destinationLatitude, destinationLongitude, destinationLocationName);
        this.sourceLocation = new OrderLocation(sourceLatitude, sourceLongitude, sourceLocationName);
    }


    public String getId() {
        return id;
    }

    public String getService_type() {
        return service_type;
    }

    public OrderLocation getDestinationLocation() {
        return destinationLocation;
    }

    public OrderLocation getSourceLocation() {
        return sourceLocation;
    }

    public String getStatus() { return status; }

    public Employee getDriverModel() {
        return driverModel;
    }

    public Long getService_time() {
        return service_time;
    }

    public User getUserModel() {
        return userModel;
    }


}
