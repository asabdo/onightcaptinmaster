package com.android.wanetcaptin.models;

/**
 * Created by Samer AlShurafa on 2/18/2018.
 */

public enum Status {
    ON, OFF
}
