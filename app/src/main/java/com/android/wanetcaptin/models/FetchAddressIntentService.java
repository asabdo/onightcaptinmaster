package com.android.wanetcaptin.models;

import android.app.Service;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.IBinder;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Samer AlShurafa on 2/15/2018.
 */


public class FetchAddressIntentService extends Service {

    private String addressResult;
    private String countryName;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        double mLatitude = intent.getDoubleExtra("Latitude", 0);
        double mLongitude = intent.getDoubleExtra("Longitude", 0);

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;

        try {
            // Return 1 addressResult.
            addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1);
        } catch (IOException ex) {
            ExceptionHandling.exceptionOccurred("FetchAddressIntentService", "IOException" + ex.getMessage());
        }catch (IllegalArgumentException ex) {
            ExceptionHandling.exceptionOccurred("FetchAddressIntentService", "IllegalArgumentException" + ex.getMessage());
        }

        if (addresses != null && addresses.size() > 0) {
            Address address = addresses.get(0);

            // Format the first line of addressResult (if available), a street addressResult, city, and country name
            if(address.getMaxAddressLineIndex() != -1)
                addressResult = String.format("%s",
                        address.getAddressLine(0)); // ,
                        //address.getLocality(), // city
                        //address.getCountryName());
            else if(address.getLocality() != null)
                addressResult = String.format("%s, %s",
                        address.getLocality(), // city
                        address.getCountryName());
            else
                addressResult = String.format("%s",
                        address.getCountryName());

            countryName = address.getCountryName();

        }

        Intent intentOutput = new Intent(AppConstants.FETCH_ADDRESS_INTENT_ACTION);
        intentOutput.putExtra(AppConstants.FETCH_ADDRESS_RESULT, addressResult);
        intentOutput.putExtra(AppConstants.FETCH_ADDRESS_COUNTRY, countryName);
        sendBroadcast(intentOutput);


        return super.onStartCommand(intent, flags, startId);
    }


}






