package com.android.wanetcaptin.models;

/**
 * Created by Samer AlShurafa on 2/1/2018.
 */

public class User {

    private String mobile_no;
    private String id;
    private String name;
    private String email;
    private String avatar;

    public User() { }

    public User(String id, String mobile_no) {
        this.id = id;
        this.mobile_no = mobile_no;
        this.name = "";
        this.email = "";
        this.avatar = "";
    }


    public String getId() { return id; }

    public String getMobile_no() {
        return mobile_no;
    }


    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAvatar() {
        return avatar;
    }


}
