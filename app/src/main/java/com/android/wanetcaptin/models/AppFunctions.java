package com.android.wanetcaptin.models;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.format.DateFormat;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Samer AlShurafa on 2/22/2018.
 */


public class AppFunctions {


    private static ArrayList<RequestLocation> listPoints = new ArrayList<>();


    public static void addToListPoints(int index, LatLng latLng, String locationName) {
        listPoints.set(index, new RequestLocation(latLng, locationName));
    }

    public static void addToListPoints(LatLng latLng, String locationName) {
        listPoints.add(new RequestLocation(latLng, locationName));
    }

    public static RequestLocation getFromListPoints(int index) {
        return listPoints.get(index);
    }

    public static int getListPointsSize() {
        return listPoints.size();
    }

    public static void clearListPoints() {
        if(listPoints.size() > 0)
            listPoints.clear();
    }





    public static String getRequestURL(LatLng origin, LatLng wayPoint, LatLng destination) {
        // origin to wayPoint, wayPoint to destination (origin => destination throw wayPoint)

        // Value of origin (origin = current)
        String str_org = "origin=" + origin.latitude + "," + origin.longitude;
        // Value of destination
        String str_destination = "destination=" + destination.latitude + "," + destination.longitude;
        // Set value enable the sensor
        String sensor = "sensor=false";
        // Mode for find direction
        String mode = "mode-driving";
        // value of Way point
        String way_point = "waypoints=" + wayPoint.latitude + "," + wayPoint.longitude;

        // Build the full param
        String param = str_org + "&" + str_destination + "&" + way_point + "&" + sensor + "&" + mode;
        // Output format
        String output = "json";

        // Create url to request
        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + param;
    }


    public static String getRequestURL(LatLng origin, LatLng destination) {
        // Value of origin (origin = current)
        String str_org = "origin=" + origin.latitude + "," + origin.longitude;
        // Value of destination
        String str_destination = "destination=" + destination.latitude + "," + destination.longitude;
        // Set value enable the sensor
        String sensor = "sensor=false";
        // Mode for find direction
        String mode = "mode-driving";
        // value of Way point
        // Build the full param
        String param = str_org + "&" + str_destination + "&" + sensor + "&" + mode;
        // Output format
        String output = "json";

        // Create url to request
        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + param;
    }


    public static String getTimeDate(long timeStamp) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timeStamp * 1000L);
        return DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString();
    }



    private Context mContext;

    public AppFunctions(Context context) {
        this.mContext = context;
    }


    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;

        try {
            if(cm != null)
                netInfo = cm.getActiveNetworkInfo();
        }catch (NullPointerException ex) {
            ExceptionHandling.exceptionOccurred("AppConstants", "NullPointerException" + ex.getMessage());
        }

        return (netInfo != null) && netInfo.isConnectedOrConnecting();
    }



}
