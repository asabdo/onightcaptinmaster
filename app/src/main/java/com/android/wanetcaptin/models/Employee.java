package com.android.wanetcaptin.models;

/**
 * Created by Samer AlShurafa on 2/6/2018.
 */


public class Employee {


    private String id;
    private String name;
    private String email;
    private String avatar;
    private String mobile_no;
    private String serviceType;
    private String status;
    private String status_type;
    private String car_model;
    private String car_no;
    private EmployeeLocation location;


    public Employee() {}

    public Employee(String id, String name, String email, String avatar, String mobile_no, String serviceType, String status,
                    String status_type, String carModel, String carNumber, EmployeeLocation location) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.avatar = avatar;
        this.mobile_no = mobile_no;
        this.serviceType = serviceType;
        this.status = status;
        this.status_type = status_type;
        this.car_model = carModel;
        this.car_no = carNumber;
        this.location = location;
    }


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public String getServiceType() {
        return serviceType;
    }

    public String getStatus() {
        return status;
    }

    public String getStatus_type() {
        return status_type;
    }

    public String getCar_model() {
        return car_model;
    }

    public String getCar_no() {
        return car_no;
    }

    public EmployeeLocation getLocation() { return location; }

}
