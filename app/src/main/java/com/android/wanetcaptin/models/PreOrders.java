package com.android.wanetcaptin.models;

/**
 * Created by Samer AlShurafa on 2/11/2018.
 */


public class PreOrders {

    private String id;
    private String service_type;
    private OrderLocation destinationLocation;
    private OrderLocation sourceLocation;

    private String status;
    private Employee driverModel;
    private User userModel;


    public PreOrders() { }

    public PreOrders(String id, String service_type, OrderLocation destinationLocation, OrderLocation sourceLocation) {
        this.id = id;
        this.service_type = service_type;
        this.destinationLocation = destinationLocation;
        this.sourceLocation = sourceLocation;
    }



    public String getId() {
        return id;
    }

    public String getService_type() {
        return service_type;
    }

    public OrderLocation getDestinationLocation() {
        return destinationLocation;
    }

    public OrderLocation getSourceLocation() {
        return sourceLocation;
    }

    public String getStatus() {
        return status;
    }

    public Employee getDriverModel() {
        return driverModel;
    }

    public User getUserModel() {
        return userModel;
    }
}
