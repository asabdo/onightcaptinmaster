package com.android.wanetcaptin.models;

import com.google.firebase.crash.FirebaseCrash;

/**
 * Created by Samer AlShurafa on 1/21/2018.
 */


public class ExceptionHandling extends Exception {


    public static void exceptionOccurred(String source, String message){

        FirebaseCrash.report(new Exception(source + " " + message));

    }

}
