package com.android.wanetcaptin.models;

/**
 * Created by Samer AlShurafa on 2/8/2018.
 */


public class OrderLocation {


    private double latitude;
    private double longitude;
    private String name;

    public OrderLocation() { }


    public OrderLocation(double latitude, double longitude, String name) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }


    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getName() {
        return name;
    }


}
