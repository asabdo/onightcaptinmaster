package com.android.wanetcaptin.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * Created by Samer AlShurafa on 1/22/2018.
 */

public class Users implements Parcelable {

    private DatabaseReference databaseReference = null;
    private User user = null;

    private String userID = "";
    private String name = "";
    private String number = "";
    private String email = "";
    private String avatar = "";


    public Users() {

    }

    protected Users(Parcel in) {
        userID = in.readString();
        name = in.readString();
        number = in.readString();
        email = in.readString();
        avatar = in.readString();
    }

    public static final Creator<Users> CREATOR = new Creator<Users>() {
        @Override
        public Users createFromParcel(Parcel in) {
            return new Users(in);
        }

        @Override
        public Users[] newArray(int size) {
            return new Users[size];
        }
    };

    public static Users getInstance() {
        return new Users();
    }


    public void setUserID(String userID) {
        this.userID = userID;

        databaseReference = FirebaseDatabase.getInstance().getReference().child(AppConstants.DB_USERS_CHILD).child(userID);
        databaseReference.addListenerForSingleValueEvent(userListener);
    }

    public void setUserPhoneNumber(String phoneNo) {
        this.number = phoneNo;
    }



    ValueEventListener userListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            user = dataSnapshot.getValue(User.class);

            if(user != null) {
                name = user.getName();
                email = user.getEmail();
                avatar = user.getAvatar();
            }

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {}
    };




    public boolean ifUserSignIn() {
        return (databaseReference != null);
    }


    public String getUserID() {
        return this.userID;
    }

    public String getUserMobileNo() {
        return this.number;
    }


    public String getUserName() {
        return name;
    }

    public String getUserEmail() {
        return email;
    }

    public String getUserPhotoUrl() {
        return avatar;
    }







    public boolean updateUserProfile(String userName, String userEmail) {
        if(databaseReference == null)
            databaseReference = FirebaseDatabase.getInstance().getReference().child(AppConstants.DB_USERS_CHILD).child(this.userID);

        try {
            databaseReference.child(AppConstants.DB_NAME_CHILD).setValue(userName);
            databaseReference.child(AppConstants.DB_EMAIL_CHILD).setValue(userEmail);
        } catch (Exception ex) {
            ExceptionHandling.exceptionOccurred("Users", "Exception: " + ex.getMessage());
            return false;
        }

        return true;
    }

    public boolean updateUserProfile(String profileImage) {
        if(databaseReference == null)
            databaseReference = FirebaseDatabase.getInstance().getReference().child(AppConstants.DB_USERS_CHILD).child(this.userID);

        try {
            databaseReference.child(AppConstants.DB_AVATAR_CHILD).setValue(profileImage);
        } catch (Exception ex) {
            ExceptionHandling.exceptionOccurred("Users", "Exception: " + ex.getMessage());
            return false;
        }

        return true;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userID);
        dest.writeString(name);
        dest.writeString(number);
        dest.writeString(email);
        dest.writeString(avatar);
    }


}





