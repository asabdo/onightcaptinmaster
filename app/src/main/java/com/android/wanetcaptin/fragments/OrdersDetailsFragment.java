package com.android.wanetcaptin.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.wanetcaptin.R;
import com.android.wanetcaptin.models.AppFunctions;
import com.android.wanetcaptin.models.ExceptionHandling;
import com.android.wanetcaptin.models.AppConstants;
import com.android.wanetcaptin.models.LoadFragmentListener;
import com.android.wanetcaptin.models.Orders;
import com.squareup.picasso.Picasso;


public class OrdersDetailsFragment extends Fragment {


    private LoadFragmentListener loadFragmentListener;
    private Orders orders = null;


    public OrdersDetailsFragment() {
    }

    public static OrdersDetailsFragment newInstance() {
        return new OrdersDetailsFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof LoadFragmentListener) {
            loadFragmentListener = (LoadFragmentListener) context;
        }
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_orders_details, container, false);
    }


    public void setOrders(Orders orders) {
        this.orders = orders;
        updateUI();
    }


    private TextView sourceLocationName, destinationLocationName, userOrderName, orderDate;
    private ImageView userOrderImage;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sourceLocationName = view.findViewById(R.id.sourceLocationName);
        destinationLocationName = view.findViewById(R.id.destinationLocationName);
        orderDate = view.findViewById(R.id.orderDate);
        userOrderName = view.findViewById(R.id.userOrderName);

        userOrderImage = view.findViewById(R.id.userOrderImage);
        ImageView callImage = view.findViewById(R.id.callImage);

        Button returnToOrdersList = view.findViewById(R.id.return_to_orders_list);

        returnToOrdersList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFragmentListener.loadFragment("OrdersFragment");
            }
        });


        callImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // open telephone application and contact user ..

                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + orders.getUserModel().getMobile_no()));
                    startActivity(intent);
                } catch (Exception ex) {
                    ExceptionHandling.exceptionOccurred("OrdersDetailsFragment", "Exception: " + ex.getMessage());
                }
            }
        });

    } // end onViewCreated


    private void updateUI() {

        if (orders != null) {

            sourceLocationName.setText(orders.getSourceLocation().getName());
            destinationLocationName.setText(orders.getDestinationLocation().getName());
            orderDate.setText(AppFunctions.getTimeDate(orders.getService_time()));
            userOrderName.setText(orders.getUserModel().getName());

            // User image :
            if (!orders.getUserModel().getAvatar().isEmpty() &&
                    !orders.getUserModel().getAvatar().equalsIgnoreCase(AppConstants.DB_AVATAR_CHILD_DEFAULT)) {

                Picasso.with(getContext())
                        .load(orders.getUserModel().getAvatar())
                        .placeholder(R.drawable.profile_user)
                        .error(R.drawable.logo_orange)
                        .into(userOrderImage);

            }

        }

    } // end updateUI

}

