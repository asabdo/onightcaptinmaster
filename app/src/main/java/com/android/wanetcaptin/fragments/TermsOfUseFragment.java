package com.android.wanetcaptin.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.android.wanetcaptin.R;


public class TermsOfUseFragment extends Fragment {

    public TermsOfUseFragment() {}

    public static TermsOfUseFragment newInstance() {
        return new TermsOfUseFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_terms_of_use, container, false);

        WebView termsContent2 = view.findViewById(R.id.termsContent2);
        WebSettings webSetting = termsContent2.getSettings();
        webSetting.setBuiltInZoomControls(true);
        webSetting.setJavaScriptEnabled(true);

        termsContent2.setWebViewClient(new WebViewClient());
        termsContent2.loadUrl("file:///android_asset/conditions.htm");

        return view;
    }

}
