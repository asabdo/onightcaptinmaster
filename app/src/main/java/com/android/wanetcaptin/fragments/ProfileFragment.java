package com.android.wanetcaptin.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.wanetcaptin.R;
import com.android.wanetcaptin.models.AppFunctions;
import com.android.wanetcaptin.models.Employee;
import com.android.wanetcaptin.models.ExceptionHandling;
import com.android.wanetcaptin.models.AppConstants;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;


public class ProfileFragment extends Fragment {

    private Uri uriFilePath = null;

    private DatabaseReference databaseReference = null;
    private StorageReference storageReference = null;

    private EditText profileNameEdit, profileEmailEdit, profileCarModelEdit, profileCarNumberEdit;
    private CircularImageView profileImageEdit;

    private SharedPreferences sharedPreferences = null;
    private SharedPreferences.Editor editor = null;


    public ProfileFragment() {}

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }





    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        profileNameEdit = view.findViewById(R.id.profileNameEdit);
        profileEmailEdit = view.findViewById(R.id.profileEmailEdit);
        profileCarModelEdit = view.findViewById(R.id.profileCarModelEdit);
        profileCarNumberEdit = view.findViewById(R.id.profileCarNumberEdit);
        EditText profilePhoneNoEdit = view.findViewById(R.id.profilePhoneNoEdit);
        profileImageEdit = view.findViewById(R.id.profileImageEdit);
        Button profileEditBtn = view.findViewById(R.id.profileEditBtn);


        if(getActivity() != null) {
            sharedPreferences = getActivity().getSharedPreferences(AppConstants.FILE_NAME, MODE_PRIVATE);
        }

        if(sharedPreferences != null) {
            editor = sharedPreferences.edit();

            String driverMobileNo = sharedPreferences.getString(AppConstants.DB_MOBILE_NO_CHILD, AppConstants.DEFAULT_VALUE);
            if(!driverMobileNo.equalsIgnoreCase(AppConstants.DEFAULT_VALUE))
                profilePhoneNoEdit.setText(driverMobileNo);
        }


        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if(currentUser != null) {
            // For update driver info
            databaseReference = FirebaseDatabase.getInstance().getReference(AppConstants.DB_EMPLOYEES_CHILD)
                    .child(currentUser.getUid());

            // For upload the image
            storageReference = FirebaseStorage.getInstance().getReference().child(
                    AppConstants.STORAGE_UPLOAD_EMPLOYEE_IMAGES_FOLDER_NAME
                            + currentUser.getUid()
                            + AppConstants.STORAGE_IMAGE_EXTENSION_TYPE);
        }



        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Employee employee = dataSnapshot.getValue(Employee.class);

                if (employee != null) {
                    if (employee.getName() != null && !employee.getName().isEmpty()) {
                        profileNameEdit.setText(employee.getName());
                        profileNameEdit.setTextColor(view.getResources().getColor(R.color.color_main_purple));
                    } else {
                        profileNameEdit.setHint(getResources().getString(R.string.menu_edit_your_data));
                        profileNameEdit.setHintTextColor(view.getResources().getColor(R.color.color_red));
                    }

                    if (employee.getEmail() != null && !employee.getEmail().isEmpty()) {
                        profileEmailEdit.setText(employee.getEmail());
                        profileEmailEdit.setTextColor(view.getResources().getColor(R.color.color_main_purple));
                    } else {
                        profileEmailEdit.setHint(getResources().getString(R.string.menu_edit_your_data));
                        profileEmailEdit.setHintTextColor(view.getResources().getColor(R.color.color_red));
                    }

                    if (employee.getCar_model() != null && !employee.getCar_model().isEmpty()) {
                        profileCarModelEdit.setText(employee.getCar_model());
                        profileCarModelEdit.setTextColor(view.getResources().getColor(R.color.color_main_purple));
                    } else {
                        profileCarModelEdit.setHint(getResources().getString(R.string.menu_edit_your_data));
                        profileCarModelEdit.setHintTextColor(view.getResources().getColor(R.color.color_red));
                    }

                    if (employee.getCar_no() != null && !employee.getCar_no().isEmpty()) {
                        profileCarNumberEdit.setText(employee.getCar_no());
                        profileCarNumberEdit.setTextColor(view.getResources().getColor(R.color.color_main_purple));
                    } else {
                        profileCarNumberEdit.setHint(getResources().getString(R.string.menu_edit_your_data));
                        profileCarNumberEdit.setHintTextColor(view.getResources().getColor(R.color.color_red));
                    }

                    // change profile image
                    if(employee.getAvatar() != null && !employee.getAvatar().equalsIgnoreCase(AppConstants.DB_AVATAR_CHILD_DEFAULT)) {

                        Picasso.with(getContext())
                                .load(employee.getAvatar())
                                .placeholder(R.drawable.profile_user)
                                .error(R.drawable.logo_orange)
                                .into(profileImageEdit);
                    }


                    if(employee.getName() != null && employee.getEmail() != null && employee.getAvatar() != null &&
                            sharedPreferences != null) {
                        // re-save the information to shared ..
                        String name = sharedPreferences.getString(AppConstants.DB_NAME_CHILD, AppConstants.DEFAULT_VALUE);
                        String email = sharedPreferences.getString(AppConstants.DB_EMAIL_CHILD, AppConstants.DEFAULT_VALUE);
                        String avatar = sharedPreferences.getString(AppConstants.DB_AVATAR_CHILD,
                                AppConstants.DB_AVATAR_CHILD_DEFAULT);
                        String carModel = sharedPreferences.getString(AppConstants.DB_CAR_MODEL_CHILD, AppConstants.DEFAULT_VALUE);
                        String carNumber = sharedPreferences.getString(AppConstants.DB_CAR_NUMBER_CHILD,
                                AppConstants.DB_AVATAR_CHILD_DEFAULT);

                        if(name.equalsIgnoreCase(AppConstants.DEFAULT_VALUE))
                            editor.putString(AppConstants.DB_NAME_CHILD, employee.getName());
                        if(email.equalsIgnoreCase(AppConstants.DEFAULT_VALUE))
                            editor.putString(AppConstants.DB_EMAIL_CHILD, employee.getEmail());
                        if(avatar.equalsIgnoreCase(AppConstants.DB_AVATAR_CHILD_DEFAULT))
                            editor.putString(AppConstants.DB_AVATAR_CHILD, employee.getAvatar());
                        if(carModel.equalsIgnoreCase(AppConstants.DEFAULT_VALUE))
                            editor.putString(AppConstants.DB_CAR_MODEL_CHILD, employee.getCar_model());
                        if(carNumber.equalsIgnoreCase(AppConstants.DB_AVATAR_CHILD_DEFAULT))
                            editor.putString(AppConstants.DB_CAR_NUMBER_CHILD, employee.getCar_no());

                        editor.apply();
                    }


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) { }

        }); // end listener




        final AppFunctions appFunctions = new AppFunctions(getContext());

        if(!appFunctions.isOnline()) { // check driver information within offline ..

            if(sharedPreferences != null) {

                String name = sharedPreferences.getString(AppConstants.DB_NAME_CHILD, AppConstants.DEFAULT_VALUE);
                String email = sharedPreferences.getString(AppConstants.DB_EMAIL_CHILD, AppConstants.DEFAULT_VALUE);
                String avatar = sharedPreferences.getString(AppConstants.DB_AVATAR_CHILD, AppConstants.DB_AVATAR_CHILD_DEFAULT);
                String carModel = sharedPreferences.getString(AppConstants.DB_CAR_MODEL_CHILD, AppConstants.DEFAULT_VALUE);
                String carNumber = sharedPreferences.getString(AppConstants.DB_CAR_NUMBER_CHILD,
                        AppConstants.DB_AVATAR_CHILD_DEFAULT);

                if (!name.equalsIgnoreCase(AppConstants.DEFAULT_VALUE)) {
                    profileNameEdit.setText(name);
                    profileNameEdit.setTextColor(view.getResources().getColor(R.color.color_main_purple));
                } else {
                    profileNameEdit.setHint(getResources().getString(R.string.menu_edit_your_data));
                    profileNameEdit.setHintTextColor(view.getResources().getColor(R.color.color_red));
                }

                if (!email.equalsIgnoreCase(AppConstants.DEFAULT_VALUE)) {
                    profileEmailEdit.setText(email);
                    profileEmailEdit.setTextColor(view.getResources().getColor(R.color.color_main_purple));
                } else {
                    profileEmailEdit.setHint(getResources().getString(R.string.menu_edit_your_data));
                    profileEmailEdit.setHintTextColor(view.getResources().getColor(R.color.color_red));
                }

                if (!carModel.equalsIgnoreCase(AppConstants.DEFAULT_VALUE)) {
                    profileCarNumberEdit.setText(carModel);
                    profileCarModelEdit.setTextColor(view.getResources().getColor(R.color.color_main_purple));
                } else {
                    profileCarNumberEdit.setHint(getResources().getString(R.string.menu_edit_your_data));
                    profileCarNumberEdit.setHintTextColor(view.getResources().getColor(R.color.color_red));
                }

                if (!carNumber.equalsIgnoreCase(AppConstants.DEFAULT_VALUE)) {
                    profileCarNumberEdit.setText(carNumber);
                    profileCarNumberEdit.setTextColor(view.getResources().getColor(R.color.color_main_purple));
                } else {
                    profileCarNumberEdit.setHint(getResources().getString(R.string.menu_edit_your_data));
                    profileCarNumberEdit.setHintTextColor(view.getResources().getColor(R.color.color_red));
                }

                if (!avatar.equalsIgnoreCase(AppConstants.DB_AVATAR_CHILD_DEFAULT)) {

                    Picasso.with(getContext())
                            .load(avatar)
                            .placeholder(R.drawable.profile_user)
                            .error(R.drawable.logo_orange)
                            .into(profileImageEdit);

                }

            }

        } // if not online



        // Open the gallery ..
        profileImageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });


        profileEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // first step is check the internet connection ..
                if(!appFunctions.isOnline()) {
                    showAlertDialog(getResources().getString(R.string.no_internet_connection),
                            getResources().getString(R.string.no_internet_connection_description));
                } else if(profileNameEdit.getText().toString().trim().isEmpty()) {
                    profileNameEdit.setError(view.getResources().getString(R.string.error_enter_user_name));
                } else if(profileEmailEdit.getText().toString().trim().isEmpty()) {
                    profileEmailEdit.setError(view.getResources().getString(R.string.error_enter_user_email));
                } else if(!ifEmailCorrect(profileEmailEdit.getText().toString())) { // not correct ..
                    profileEmailEdit.setError(view.getResources().getString(R.string.error_email_in_correct));
                } else if(profileCarModelEdit.getText().toString().trim().isEmpty()) {
                    profileCarModelEdit.setError(view.getResources().getString(R.string.error_enter_car_model));
                } else if(profileCarNumberEdit.getText().toString().trim().isEmpty()) {
                    profileCarNumberEdit.setError(view.getResources().getString(R.string.error_enter_car_number));
                } else {
                    // Update the Firebase database .. name, email, car model, car number
                    String name = profileNameEdit.getText().toString().trim();
                    String email = profileEmailEdit.getText().toString().trim();
                    String carModel = profileCarModelEdit.getText().toString().trim();
                    String carNumber = profileCarNumberEdit.getText().toString().trim();

                    databaseReference.child(AppConstants.DB_NAME_CHILD).setValue(name);
                    databaseReference.child(AppConstants.DB_EMAIL_CHILD).setValue(email);
                    databaseReference.child(AppConstants.DB_CAR_MODEL_CHILD).setValue(carModel);
                    databaseReference.child(AppConstants.DB_CAR_NUMBER_CHILD).setValue(carNumber);

                    //boolean update_result = the_user.updateUserProfile(name, email);

                    //if(update_result){

                    if(sharedPreferences != null) {
                        editor.putString(AppConstants.DB_NAME_CHILD, name);
                        editor.putString(AppConstants.DB_EMAIL_CHILD, email);
                        editor.putString(AppConstants.DB_CAR_MODEL_CHILD, carModel);
                        editor.putString(AppConstants.DB_CAR_NUMBER_CHILD, carNumber);
                        editor.apply();
                    }

                    if(uriFilePath != null) {
                        uploadImage();
                    }
                    else {
                        Toast.makeText(getContext(), getResources().getString(R.string.profile_success_update_info),
                                Toast.LENGTH_SHORT).show();
                    }
                    //} else {
                    //    Toast.makeText(getContext(), getResources().getString(R.string.error_edit_info), Toast.LENGTH_SHORT).show();
                    //}
                }
            }
        });

    } // end onViewCreated






    private boolean ifEmailCorrect(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        this.startActivityForResult(Intent.createChooser(intent, "اختر صورة"), AppConstants.PROFILE_PICK_IMAGE_REQUEST);
    }





    private void showAlertDialog(String text1, String text2) {
        final AlertDialog.Builder progressDialog = new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme);

        TextView title = new TextView(getContext());
        title.setText(getResources().getString(R.string.request_service));
        title.setTextSize(20);
        title.setGravity(Gravity.CENTER_HORIZONTAL);
        title.setPadding(0, 40, 0, 0);
        title.setTextColor(getResources().getColor(R.color.color_main_purple));

        TextView messageText = new TextView(getContext());
        messageText.setText(Html.fromHtml("<br><br>" + text1 + "<br>" + text2 + "<br>" ));
        messageText.setTextSize(15);
        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
        messageText.setTextColor(getResources().getColor(R.color.color_magor_purple));

        progressDialog.setCustomTitle(title);
        progressDialog.setView(messageText);
        //progressDialog.setCancelable(false);
        progressDialog.setPositiveButton("حسنا",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                dialog.cancel();
            }
        });

        progressDialog.show();
    }





    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // Hide the keyboard if it is show before go to another fragment

        final InputMethodManager imm;
        try {
            if (getActivity() != null) {
                imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null && getView() != null) {
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                }
            }
        }catch (NullPointerException ex) {
            ExceptionHandling.exceptionOccurred("ProfileFragment", "NullPointerException: " + ex.getMessage());
        }
    }






    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == AppConstants.PROFILE_PICK_IMAGE_REQUEST && resultCode == RESULT_OK &&
                intent != null && intent.getData() != null) {

            uriFilePath = intent.getData();

            try {
                Bitmap bitmap = null;
                if(getActivity() != null) {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uriFilePath);
                }
                if(bitmap != null) {
                    profileImageEdit.setImageBitmap(bitmap);
                }

            } catch (IOException e) {
                ExceptionHandling.exceptionOccurred("ProfileFragment", "IOException: " + e.getMessage());
            }

        }
    }





    private void uploadImage() {

        if (uriFilePath != null) {
            final ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle(getResources().getString(R.string.profile_edit_info));
            progressDialog.show();

            // Reduce the size of image ..

            Bitmap bitmap = null;

            try {
                if(getActivity() != null)
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uriFilePath);
            } catch (IOException e) {
                ExceptionHandling.exceptionOccurred("ProfileFragment", "IOException: " + e.getMessage());
            }

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            if(bitmap != null)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);

            //final BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inSampleSize = 2;
            // Decode bitmap with inSampleSize set
            //options.inJustDecodeBounds = false;

            //Bitmap newBitmap = null;

            //if(bitmap != null)
            //  newBitmap = Bitmap.createScaledBitmap(bitmap, 200, 200, true);


            //byte[] imageBytes = byteArrayOutputStream.toByteArray();
            //String newPath = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap,
            //        "Title", null);
            //Uri newFile;
            UploadTask uploadTask;

            //newFile = Uri.fromFile(f);

            //if(newPath != null) {
            //Toast.makeText(getContext(), "Real Path: " + realPath, Toast.LENGTH_LONG).show();
            //newFile = Uri.parse(newPath);
            //uploadTask = storageReference.putFile(newFile);
            //} else {
            //Toast.makeText(getContext(), "New Path null", Toast.LENGTH_LONG).show();

            //}


            uploadTask = storageReference.putFile(uriFilePath);

            uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                    progressDialog.setMessage("قيد التنفيذ " + (int) progress + "%");
                }
            }).addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    ExceptionHandling.exceptionOccurred("ProfileFragment", "Exception: " + exception.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    if(taskSnapshot.getMetadata() != null) {

                        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                String downloadUrlPath = "";

                                if(uri != null) {
                                    downloadUrlPath = uri.toString();
                                }

                                if(sharedPreferences != null && !downloadUrlPath.isEmpty()) {
                                    editor.putString(AppConstants.DB_AVATAR_CHILD, downloadUrlPath);
                                    editor.apply();
                                }

                                if(!downloadUrlPath.isEmpty())
                                    databaseReference.child(AppConstants.DB_AVATAR_CHILD).setValue(downloadUrlPath);

                                if(progressDialog.isShowing()) {
                                    progressDialog.hide();
                                    progressDialog.dismiss();
                                }

                                Toast.makeText(getContext(), getResources().getString(R.string.profile_success_update_info),
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            });


        } // end if file path != null

    } // end uploadImage






}



