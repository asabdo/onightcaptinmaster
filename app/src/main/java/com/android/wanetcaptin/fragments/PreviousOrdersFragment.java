package com.android.wanetcaptin.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.wanetcaptin.R;
import com.android.wanetcaptin.adapters.OrdersAdapter;
import com.android.wanetcaptin.models.AppConstants;
import com.android.wanetcaptin.models.LoadFragmentListener;
import com.android.wanetcaptin.models.Orders;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class PreviousOrdersFragment extends Fragment {


    private LinearLayout noPreviousOrders;
    private RecyclerView previousOrdersRecyclerView;

    private ArrayList<Orders> ordersList;
    private OrdersAdapter ordersAdapter;
    private DatabaseReference ordersDatabaseReference = null;

    private LoadFragmentListener loadFragmentListener;


    public PreviousOrdersFragment() { }

    public static PreviousOrdersFragment newInstance() {
        return new PreviousOrdersFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof LoadFragmentListener) {
            loadFragmentListener = (LoadFragmentListener) context;
        }
    }




    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orders_previous, container, false);

        noPreviousOrders = view.findViewById(R.id.noPreviousOrders);
        previousOrdersRecyclerView = view.findViewById(R.id.previousOrdersRecyclerView);
        TextView orderNow_Previous = view.findViewById(R.id.orderNow_Previous);


        ordersDatabaseReference = FirebaseDatabase.getInstance().getReference(AppConstants.DB_ORDERS_CHILD);


        ordersList = new ArrayList<>();
        ordersAdapter = new OrdersAdapter(ordersList, getContext());

        previousOrdersRecyclerView.setAdapter(ordersAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        previousOrdersRecyclerView.setLayoutManager(linearLayoutManager);

        if(ordersList.size() > 0) { // there is orders ..
            noPreviousOrders.setVisibility(View.GONE);
            previousOrdersRecyclerView.setVisibility(View.VISIBLE);
        } else {
            noPreviousOrders.setVisibility(View.VISIBLE);
            previousOrdersRecyclerView.setVisibility(View.GONE);
        }




        orderNow_Previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragmentListener.loadFragment("MapsFragment");
            }
        });

        return view;
    }





    @Override
    public void onStart() {
        super.onStart();

        // Get driver id ..
        String driverId = "";

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if(currentUser != null) {
            driverId = currentUser.getUid();
        }

        String driver_status = driverId + "_" + AppConstants.DB_STATUS_CHILD_COMPLETED;

        // Apply filter ..
        ordersDatabaseReference.orderByChild(AppConstants.DB_ORDERS_DRIVER_STATUS_CHILD).equalTo(driver_status)
                .addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        ordersList.clear();

                        Orders orders;

                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            orders = postSnapshot.getValue(Orders.class);

                            // Query: order != null

                            if(orders != null) {

                                ordersList.add(orders);

                            }
                        }


                        if(ordersList.size() > 0) {  // there is orders ..
                            ordersAdapter = new OrdersAdapter(ordersList, getContext());
                            previousOrdersRecyclerView.setAdapter(ordersAdapter);
                            ordersAdapter.notifyDataSetChanged();

                            noPreviousOrders.setVisibility(View.GONE);
                            previousOrdersRecyclerView.setVisibility(View.VISIBLE);
                        } else {
                            noPreviousOrders.setVisibility(View.VISIBLE);
                            previousOrdersRecyclerView.setVisibility(View.GONE);
                        }

                    } // end onDataChange

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}

                });

    } // end onStart


}




