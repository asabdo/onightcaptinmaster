package com.android.wanetcaptin.fragments;

        import android.annotation.SuppressLint;
        import android.content.Context;
        import android.graphics.Color;
        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.support.annotation.NonNull;
        import android.support.annotation.Nullable;
        import android.support.v4.app.Fragment;
        import android.support.v7.widget.LinearLayoutManager;
        import android.support.v7.widget.RecyclerView;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageView;

        import com.android.wanetcaptin.R;
        import com.android.wanetcaptin.adapters.OrdersStatusAdapter;
        import com.android.wanetcaptin.models.AppFunctions;
        import com.android.wanetcaptin.models.DirectionsParser;
        import com.android.wanetcaptin.models.Employee;
        import com.android.wanetcaptin.models.EmployeeLocation;
        import com.android.wanetcaptin.models.ExceptionHandling;
        import com.android.wanetcaptin.models.AppConstants;
        import com.android.wanetcaptin.models.LoadFragmentListener;
        import com.android.wanetcaptin.models.OrderLocation;
        import com.android.wanetcaptin.models.Orders;
        import com.android.wanetcaptin.models.OrdersListener;
        import com.android.wanetcaptin.models.Status;
        import com.android.wanetcaptin.models.TimeLineStatus;

        import com.google.android.gms.maps.CameraUpdate;
        import com.google.android.gms.maps.CameraUpdateFactory;
        import com.google.android.gms.maps.GoogleMap;
        import com.google.android.gms.maps.MapView;
        import com.google.android.gms.maps.MapsInitializer;
        import com.google.android.gms.maps.OnMapReadyCallback;
        import com.google.android.gms.maps.model.BitmapDescriptorFactory;
        import com.google.android.gms.maps.model.LatLng;
        import com.google.android.gms.maps.model.MarkerOptions;
        import com.google.android.gms.maps.model.PolylineOptions;
        import com.google.firebase.database.DataSnapshot;
        import com.google.firebase.database.DatabaseError;
        import com.google.firebase.database.FirebaseDatabase;
        import com.google.firebase.database.ValueEventListener;
        import com.google.maps.android.ui.IconGenerator;

        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.BufferedReader;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.net.HttpURLConnection;
        import java.net.MalformedURLException;
        import java.net.URL;
        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.List;


    public class OrdersTimeLineFragment extends Fragment implements OnMapReadyCallback {

        private ValueEventListener valueEventListener_Orders = null, valueEventListener_Employees = null;

        private OrdersListener ordersListener;
        private Orders orders;

        private MapView mapTimeLineView;
        private GoogleMap mMap;
        private MarkerOptions markerOptions;

        private String responseString = "";


        public OrdersTimeLineFragment() {  }

        public static OrdersTimeLineFragment newInstance() {
            return new OrdersTimeLineFragment();
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_orders_time_line, container, false);
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);

            if(context instanceof LoadFragmentListener) {
                ordersListener = (OrdersListener) context;
            }
        }



        private OrdersStatusAdapter ordersStatusAdapter;
        private ArrayList<TimeLineStatus> arrayTimeLineList = new ArrayList<>(6);


        public void setOrders(Orders orders) {
            this.orders = orders;

            // every time open the fragment .. reset the list ..
            arrayTimeLineList.clear();

            initializeTimeLineList();
            updateTimeLineList();

            drawOrderRoute();

            startFirebaseDatabase();
        }



        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            if(getContext() == null)
                return;


            RecyclerView timeLineListRecycler = view.findViewById(R.id.timeLineRecycler);
            arrayTimeLineList = initializeTimeLineList();
            ordersStatusAdapter = new OrdersStatusAdapter(arrayTimeLineList);
            timeLineListRecycler.setAdapter(ordersStatusAdapter);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            timeLineListRecycler.setLayoutManager(linearLayoutManager);


            mapTimeLineView = view.findViewById(R.id.mapTimeLine);
            mapTimeLineView.onCreate(savedInstanceState);
            mapTimeLineView.onResume(); // needed to get the map to display immediately

            try {
                if(getActivity() != null)
                    MapsInitializer.initialize(getActivity().getApplicationContext());
            } catch (Exception e) {
                ExceptionHandling.exceptionOccurred("OrdersTimeLineFragment", "Exception: " + e.getMessage());
            }
            mapTimeLineView.getMapAsync(this);



            ImageView backToDetails = view.findViewById(R.id.backToDetails);
            backToDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ordersListener.onOrderSelected(orders, AppConstants.TIME_LINE_TO_DETAILS);
                }
            });

        } // onViewCreated ..





        private void startFirebaseDatabase() {
            if(orders == null)
                return;

            valueEventListener_Orders =
                    FirebaseDatabase.getInstance().getReference().child(AppConstants.DB_ORDERS_CHILD).child(orders.getId())
                            .addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    orders = dataSnapshot.getValue(Orders.class);
                                    updateTimeLineList();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });


            // Update driver location on map if order status not completed ..
            if(orders != null && !orders.getStatus().equalsIgnoreCase(AppConstants.DB_STATUS_CHILD_COMPLETED)) {

                valueEventListener_Employees =
                        FirebaseDatabase.getInstance().getReference().child(AppConstants.DB_EMPLOYEES_CHILD).child(orders.getDriverModel().getId())
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        Employee employee = dataSnapshot.getValue(Employee.class);
                                        updateMapMarker(employee);
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

            } // end if order status not completed ..

        } // end satartFirebaseDatabase



        @Override
        public void onDestroy() {
            super.onDestroy();

            mapTimeLineView.onDestroy();

            if(orders == null)
                return;

            FirebaseDatabase.getInstance().getReference().child(AppConstants.DB_ORDERS_CHILD).child(orders.getId())
                    .removeEventListener(valueEventListener_Orders);
            FirebaseDatabase.getInstance().getReference().child(AppConstants.DB_EMPLOYEES_CHILD).child(orders.getDriverModel().getId())
                    .removeEventListener(valueEventListener_Employees);
        }


        @Override
        public void onMapReady(GoogleMap googleMap) {
            if(mMap == null) {
                mMap = googleMap;
            }

            if (markerOptions == null) {
                markerOptions = new MarkerOptions();
            }

            mMap.getUiSettings().setZoomControlsEnabled(true);

            drawOrderRoute();
        }




        private void drawOrderRoute(){
            if(orders == null || mMap == null || markerOptions == null)
                return;

            mMap.clear();

            OrderLocation orderLocation;
            LatLng sourceLatLng, destinationLatLng;

            // Add the marker to the map
            IconGenerator iconGenerator = new IconGenerator(getContext());
            iconGenerator.setColor(Color.GREEN);

            orderLocation = orders.getSourceLocation();
            sourceLatLng = new LatLng(orderLocation.getLatitude(), orderLocation.getLongitude());

            mMap.addMarker(markerOptions
                    .position(sourceLatLng)
                    .icon(BitmapDescriptorFactory.fromBitmap(
                            iconGenerator.makeIcon(getString(R.string.transfer_from))))
                    .anchor(iconGenerator.getAnchorU(), iconGenerator.getAnchorV()));


            orderLocation = orders.getDestinationLocation();
            destinationLatLng = new LatLng(orderLocation.getLatitude(), orderLocation.getLongitude());
            iconGenerator.setColor(Color.RED);

            mMap.addMarker(markerOptions
                    .position(destinationLatLng)
                    .icon(BitmapDescriptorFactory.fromBitmap(
                            iconGenerator.makeIcon(getString(R.string.transfer_to))))
                    .anchor(iconGenerator.getAnchorU(), iconGenerator.getAnchorV()));


            String url = AppFunctions.getRequestURL(sourceLatLng, destinationLatLng);

            TaskRequestDirections taskRequestDirections = new TaskRequestDirections();
            taskRequestDirections.execute(url);

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(sourceLatLng, 18);
            mMap.animateCamera(cameraUpdate);

        }



        private void updateMapMarker(Employee employee) {
            // clear previous marker ..
            mMap.clear();

            OrderLocation orderLocation;
            LatLng sourceLatLng, destinationLatLng;

            IconGenerator iconGenerator = new IconGenerator(getContext());
            iconGenerator.setColor(Color.GREEN);

            orderLocation = orders.getSourceLocation();
            sourceLatLng = new LatLng(orderLocation.getLatitude(), orderLocation.getLongitude());

            mMap.addMarker(markerOptions
                    .position(sourceLatLng)
                    .icon(BitmapDescriptorFactory.fromBitmap(
                            iconGenerator.makeIcon(getString(R.string.transfer_from))))
                    .anchor(iconGenerator.getAnchorU(), iconGenerator.getAnchorV()));


            orderLocation = orders.getDestinationLocation();
            destinationLatLng = new LatLng(orderLocation.getLatitude(), orderLocation.getLongitude());
            iconGenerator.setColor(Color.RED);

            mMap.addMarker(markerOptions
                    .position(destinationLatLng)
                    .icon(BitmapDescriptorFactory.fromBitmap(
                            iconGenerator.makeIcon(getString(R.string.transfer_to))))
                    .anchor(iconGenerator.getAnchorU(), iconGenerator.getAnchorV()));


            EmployeeLocation employeeLocation = employee.getLocation();

            LatLng addLatLng = new LatLng(employeeLocation.getLatitude(), employeeLocation.getLongitude());

            switch (employee.getServiceType()) {
                case "motorcycle_service":
                    mMap.addMarker(markerOptions.position(addLatLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.motorcycle_service_icon)));
                    break;
                case "car_service":
                    mMap.addMarker(markerOptions.position(addLatLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_service_icon)));
                    break;
                case "truck_service":
                    mMap.addMarker(markerOptions.position(addLatLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.truck_service_icon)));
                    break;
                case "oneight_service":
                    mMap.addMarker(markerOptions.position(addLatLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.oneight_service_icon)));
                    break;
            } // end switch

            if(!responseString.isEmpty()) {
                TaskParser taskParser = new TaskParser();
                taskParser.execute(responseString); // redraw the line
            }
        } // end updateMapMarker







        private ArrayList<TimeLineStatus> initializeTimeLineList() {
            arrayTimeLineList.add(0, new TimeLineStatus(getResources().getString(R.string.time_line_new_title),
                    getResources().getString(R.string.time_line_new_description), Status.ON));

            arrayTimeLineList.add(1, new TimeLineStatus(getResources().getString(R.string.time_line_assigned_title),
                    getResources().getString(R.string.time_line_assigned_description), Status.ON));

            arrayTimeLineList.add(2, new TimeLineStatus(getResources().getString(R.string.time_line_accepted_title),
                    getResources().getString(R.string.time_line_accepted_description), Status.ON));

            arrayTimeLineList.add(3, new TimeLineStatus(getResources().getString(R.string.time_line_reached_title),
                    getResources().getString(R.string.time_line_reached_description), Status.OFF));

            arrayTimeLineList.add(4, new TimeLineStatus(getResources().getString(R.string.time_line_in_progress_title),
                    getResources().getString(R.string.time_line_in_progress_description), Status.OFF));

            TimeLineStatus timeLineStatus = new TimeLineStatus(getResources().getString(R.string.time_line_completed_title),
                    getResources().getString(R.string.time_line_completed_description), Status.OFF);
            timeLineStatus.setIfLast(true);
            arrayTimeLineList.add(5, timeLineStatus);

            return arrayTimeLineList;
        }


        private void updateTimeLineList(){
            if(orders == null)
                return;

            String status = orders.getStatus();
            TimeLineStatus timeLineStatus;

            if(status.equalsIgnoreCase(AppConstants.DB_STATUS_CHILD_COMPLETED)) {
                timeLineStatus = arrayTimeLineList.get(3); // reached
                timeLineStatus.setStatus(Status.ON);
                timeLineStatus = arrayTimeLineList.get(4); // in_progress
                timeLineStatus.setStatus(Status.ON);
                timeLineStatus = arrayTimeLineList.get(5); // completed
                timeLineStatus.setStatus(Status.ON);
            } else if(status.equalsIgnoreCase(AppConstants.DB_STATUS_CHILD_IN_PROGRESS)) {
                timeLineStatus = arrayTimeLineList.get(3); // reached
                timeLineStatus.setStatus(Status.ON);
                timeLineStatus = arrayTimeLineList.get(4); // in_progress
                timeLineStatus.setStatus(Status.ON);
            } else if(status.equalsIgnoreCase(AppConstants.DB_STATUS_CHILD_REACHED)) {
                timeLineStatus = arrayTimeLineList.get(3); // reached
                timeLineStatus.setStatus(Status.ON);
            }

            ordersStatusAdapter.notifyDataSetChanged();
        }












        @Override
        public void onResume() { super.onResume(); mapTimeLineView.onResume(); }

        @Override
        public void onPause() { super.onPause(); mapTimeLineView.onPause(); }

        @Override
        public void onStop() { super.onStop(); mapTimeLineView.onStop(); }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
        }

        @Override
        public void onLowMemory() { super.onLowMemory(); mapTimeLineView.onLowMemory(); }

        @Override
        public void onSaveInstanceState(@NonNull Bundle outState) {
            super.onSaveInstanceState(outState);
            mapTimeLineView.onSaveInstanceState(outState);
        }






        @SuppressLint("StaticFieldLeak")
        public class TaskRequestDirections extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... strings) {


                try {
                    responseString = getRequestDirection(strings[0]);
                } catch (IOException e) {
                    ExceptionHandling.exceptionOccurred("OrdersTimeLine-TaskRequestDirections", "IOException " + e.getMessage());
                    e.printStackTrace();
                }

                return responseString;
            }


            @Override
            protected void onPostExecute(String response_string) {
                super.onPostExecute(response_string);
                responseString = response_string;

                // Parse JSON here
                TaskParser taskParser = new TaskParser();
                taskParser.execute(response_string);
            }


        } // End TaskRequestDirections class



        @SuppressLint("StaticFieldLeak")
        public class TaskParser extends AsyncTask<String, Void, List<List<HashMap<String, String>>>> {

            @Override
            protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
                JSONObject jsonObject;
                List<List<HashMap<String, String>>> routes = null;

                try {
                    jsonObject = new JSONObject(strings[0]);

                    DirectionsParser directionsParser = new DirectionsParser();
                    routes = directionsParser.parse(jsonObject);
                } catch (JSONException e) {
                    ExceptionHandling.exceptionOccurred("OrdersTimeLine-TaskParser", "JSONException " + e.getMessage());
                }

                return routes;
            }

            @Override
            protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
                // Get list route and disable it into the map

                ArrayList<LatLng> points;
                PolylineOptions polylineOptions = null;
                double lat, lon;

                for (List<HashMap<String, String>> path : lists) {
                    points = new ArrayList<>();
                    polylineOptions = new PolylineOptions();

                    for (HashMap<String, String> point : path) {
                        lat = Double.parseDouble(point.get("lat"));
                        lon = Double.parseDouble(point.get("lon"));

                        points.add(new LatLng(lat, lon));
                    } // inner for
                    polylineOptions.addAll(points);
                    polylineOptions.width(18);
                    polylineOptions.color(getResources().getColor(R.color.color_main_purple));
                    polylineOptions.geodesic(true);
                } // outer for

                if (polylineOptions != null) {
                    mMap.addPolyline(polylineOptions);
                }// else {
                //Toast.makeText(getContext(), "Direction not found!", Toast.LENGTH_LONG).show();
                //}
            }

        } // End Task Parser class


        private String getRequestDirection(String requestURL) throws IOException {
            String responseString = "";

            InputStream inputStream = null;
            HttpURLConnection httpURLConnection = null;

            try {
                URL url = new URL(requestURL);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.connect();

                // Get the response result
                inputStream = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                StringBuilder stringBuilder = new StringBuilder();
                String line;

                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }

                responseString = stringBuilder.toString();
                bufferedReader.close();
                inputStreamReader.close();
            } catch (MalformedURLException e) {
                ExceptionHandling.exceptionOccurred("OrdersTimeLineFragment", "MalformedURLException " + e.getMessage());
            } catch (IOException e) {
                ExceptionHandling.exceptionOccurred("OrdersTimeLineFragment", "IOException " + e.getMessage());
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if(httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }

            return  responseString;
        } // end getRequestDirection


    }



