package com.android.wanetcaptin.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.wanetcaptin.R;
import com.android.wanetcaptin.models.AppConstants;
import com.android.wanetcaptin.models.AppFunctions;
import com.android.wanetcaptin.models.DirectionsParser;
import com.android.wanetcaptin.models.EmployeeLocation;
import com.android.wanetcaptin.models.ExceptionHandling;
import com.android.wanetcaptin.models.LoadFragmentListener;
import com.android.wanetcaptin.models.OrderLocation;
import com.android.wanetcaptin.models.Orders;
import com.android.wanetcaptin.models.OrdersListener;

import com.android.wanetcaptin.models.PreOrders;
import com.android.wanetcaptin.models.SoundService;
import com.android.wanetcaptin.models.User;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.ui.IconGenerator;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.ceryle.radiorealbutton.RadioRealButton;
import co.ceryle.radiorealbutton.RadioRealButtonGroup;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.Context.MODE_PRIVATE;
import static android.graphics.Paint.ANTI_ALIAS_FLAG;


public class MapsFragment extends Fragment implements OnMapReadyCallback {

    private RadioRealButtonGroup statusGroup;
    private FloatingActionButton actionButton;
    private MapView mMapView;

    private GoogleMap mMap;
    private MarkerOptions markerOptions;
    private Location currentLocation = null;
    private LatLng currentLatLng = null;


    private ChildEventListener childEventListener1 = null, childEventListener2 = null;
    private ValueEventListener valueEventListener = null;
    private DatabaseReference databaseReference = null;

    private Dialog invitationDialog = null;
    private Dialog closeDialog = null;
    private Dialog driverDialog = null;

    private String ifTherePendingOrder = "no";
    private String orderId = "";
    private boolean isInvitation = false;

    private OrdersListener ordersListener;

    private String responseString = "";

    private SharedPreferences sharedPreferences = null;
    private SharedPreferences.Editor editor = null;

    private PreOrders preOrders = null;
    private Orders pendingOrder = null;
    private String driverId = "";

    private String actionButtonText = "";

    private String emp_status = "";




    public MapsFragment() { }

    public static MapsFragment newInstance() {
        return new MapsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof LoadFragmentListener) {
            this.ordersListener = (OrdersListener) context;
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_maps, container, false);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.track_order_dialog) {

            if(driverDialog != null && !driverDialog.isShowing()) {
                driverDialog.show();
            } else if (pendingOrder != null) {
                showTrackOrderDialog(getContext(), pendingOrder.getUserModel());
            }

        }
        return true;
    }





    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        databaseReference = FirebaseDatabase.getInstance().getReference();

        statusGroup = view.findViewById(R.id.statusGroup);
        actionButton = view.findViewById(R.id.actionButton);
        final RadioRealButton unavailable = view.findViewById(R.id.unavailable);

        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if(currentUser != null)
            driverId = currentUser.getUid();


        statusGroup.setOnClickedButtonListener(new RadioRealButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(RadioRealButton button, int position) {
                // 0 : unavailable, 1 : available
                if(currentUser != null) {
                    if (position == 0) {
                        databaseReference.child(AppConstants.DB_EMPLOYEES_CHILD).child(driverId)
                                .child(AppConstants.DB_STATUS_CHILD).setValue(AppConstants.DB_STATUS_CHILD_UNAVAILABLE);
                    } else {
                        databaseReference.child(AppConstants.DB_EMPLOYEES_CHILD).child(driverId)
                                .child(AppConstants.DB_STATUS_CHILD).setValue(AppConstants.DB_STATUS_CHILD_AVAILABLE);
                    }
                }
            }
        });



        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppFunctions appFunctions = new AppFunctions(getContext());
                if(!appFunctions.isOnline()) {
                    showAlertDialog(getResources().getString(R.string.no_internet_connection),
                            getResources().getString(R.string.no_internet_connection_description));
                    return;
                }



                if(actionButtonText.equalsIgnoreCase(getResources().getString(R.string.actionButtonFinish))) {

                    databaseReference.child(AppConstants.DB_ORDERS_CHILD).child(orderId)
                            .child(AppConstants.DB_STATUS_CHILD).setValue(AppConstants.DB_STATUS_CHILD_COMPLETED);

                    if(editor != null) {
                        editor.remove(AppConstants.DB_ORDER_ID);
                        editor.putString(AppConstants.DB_ORDER_PENDING, "no");
                        editor.apply();
                    }

                    if(mMap != null)
                        mMap.clear();

                    isInvitation = false;
                    ifTherePendingOrder = "no";
                    orderId = "";
                    actionButton.setVisibility(View.INVISIBLE);
                    unavailable.setChecked(true);
                    statusGroup.setVisibility(View.VISIBLE);
                    showCloseOrderDialog(AppConstants.DB_STATUS_CHILD_COMPLETED);

                } else if(actionButtonText.equalsIgnoreCase(getResources().getString(R.string.actionButtonReceipt))) {

                    databaseReference.child(AppConstants.DB_ORDERS_CHILD).child(orderId)
                            .child(AppConstants.DB_STATUS_CHILD).setValue(AppConstants.DB_STATUS_CHILD_IN_PROGRESS);

                    actionButtonText = getResources().getString(R.string.actionButtonFinish);
                    actionButton.setImageBitmap(textAsBitmap(actionButtonText));

                } else if(actionButtonText.equalsIgnoreCase(getResources().getString(R.string.actionButtonArrival))) {

                    databaseReference.child(AppConstants.DB_ORDERS_CHILD).child(orderId)
                            .child(AppConstants.DB_STATUS_CHILD).setValue(AppConstants.DB_STATUS_CHILD_REACHED);

                    actionButtonText = getResources().getString(R.string.actionButtonReceipt);
                    actionButton.setImageBitmap(textAsBitmap(actionButtonText));

                } else if(actionButtonText.equalsIgnoreCase(getResources().getString(R.string.actionButtonAccept))) {

                    databaseReference.child(AppConstants.DB_PRE_ORDERS_CHILD).child(orderId)
                            .child(AppConstants.DB_STATUS_CHILD).setValue(AppConstants.DB_STATUS_CHILD_ACCEPTED);

                    ifTherePendingOrder = "yes";
                    if(editor != null) {
                        editor.putString(AppConstants.DB_ORDER_ID, orderId);
                        editor.putString(AppConstants.DB_ORDER_PENDING, "yes");
                        editor.apply();
                    }

                    actionButtonText = getResources().getString(R.string.actionButtonArrival);
                    actionButton.setImageBitmap(textAsBitmap(actionButtonText));

                    getPendingOrderObject();
                }

            }

        }); // end actionButton listener ..



        mMapView = view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately


        try {
            if(getActivity() != null)
                MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            ExceptionHandling.exceptionOccurred("MapsFragment", "Exception: " + e.getMessage());
        }

        mMapView.getMapAsync(this);





        if(getActivity() != null) {
            sharedPreferences = getActivity().getSharedPreferences(AppConstants.FILE_NAME, MODE_PRIVATE);
        }

        if(sharedPreferences != null) {
            editor = sharedPreferences.edit();
        }

        AppFunctions appFunctions = new AppFunctions(getContext());

        if(appFunctions.isOnline()) {

            statusGroup.setVisibility(View.VISIBLE);
            actionButton.setVisibility(View.INVISIBLE);

            if (currentUser != null) {

                // get all pending orders and check if there is order to this driver (max: drivers: 15 => not completed => 15) Best :)
                // get all orders to this driver and check if there is pending order to him\her (driver: 100 orders ..)

                databaseReference.child(AppConstants.DB_ORDERS_CHILD)
                        .orderByChild(AppConstants.DB_ORDER_IN_PROGRESS).equalTo(true)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Orders orders;

                                for (DataSnapshot child : dataSnapshot.getChildren()) {
                                    orders = child.getValue(Orders.class);

                                    if (orders != null && null != orders.getDriverModel()) {
                                        if (driverId.equals(orders.getDriverModel().getId())) {
                                            ifTherePendingOrder = "yes";
                                            isInvitation = true;
                                            orderId = orders.getId();
                                            pendingOrder = orders;
                                            statusGroup.setVisibility(View.INVISIBLE);
                                            actionButton.setVisibility(View.VISIBLE);
                                            String status = pendingOrder.getStatus();

                                            switch (status) {
                                                case AppConstants.DB_STATUS_CHILD_ACCEPTED:
                                                    actionButtonText = getResources().getString(R.string.actionButtonArrival);
                                                    actionButton.setImageBitmap(textAsBitmap(actionButtonText));
                                                    break;
                                                case AppConstants.DB_STATUS_CHILD_REACHED:
                                                    actionButtonText = getResources().getString(R.string.actionButtonReceipt);
                                                    actionButton.setImageBitmap(textAsBitmap(actionButtonText));
                                                    break;
                                                case AppConstants.DB_STATUS_CHILD_IN_PROGRESS:
                                                    actionButtonText = getResources().getString(R.string.actionButtonFinish);
                                                    actionButton.setImageBitmap(textAsBitmap(actionButtonText));
                                                    break;
                                            }

                                            if (editor != null) {
                                                editor.putString(AppConstants.DB_ORDER_ID, orderId);
                                                editor.putString(AppConstants.DB_ORDER_PENDING, "yes");
                                                editor.apply();
                                            }

                                            if (pendingOrder != null) {
                                                showPendingOrderOnMap(pendingOrder);
                                                showTrackOrderDialog(getContext(), pendingOrder.getUserModel());
                                            }

                                            break;
                                        }
                                    }
                                } // end for loop
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) { }

                        });
            }

        } else if(sharedPreferences != null) {

            ifTherePendingOrder = sharedPreferences.getString(AppConstants.DB_ORDER_PENDING, AppConstants.DEFAULT_VALUE);
            orderId = sharedPreferences.getString(AppConstants.DB_ORDER_ID, AppConstants.DEFAULT_VALUE);

            if(ifTherePendingOrder.equalsIgnoreCase("yes")) {
                isInvitation = true;
                statusGroup.setVisibility(View.INVISIBLE);
                actionButton.setVisibility(View.VISIBLE);
                showAlertDialog(getResources().getString(R.string.no_internet_connection),
                        getResources().getString(R.string.no_internet_pending_order));
            } else {
                ifTherePendingOrder = "no";
                isInvitation = false;
                unavailable.setChecked(true);
                statusGroup.setVisibility(View.VISIBLE);
                actionButton.setVisibility(View.INVISIBLE);
            }
        } // end if(sharedPreferences != null






        if (getContext() != null &&
                ActivityCompat.checkSelfPermission(getContext(), ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                    AppConstants.LOCATION_PERMISSION_CODE);
        }

        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(final Location location) {
                if (location != null) {
                    currentLocation = location;

                    //currentLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                    // ToDo remove this ,, for test
                    currentLatLng = new LatLng(24.6417714,46.7249202);

                    if(currentLocation != null) {
                        //latLng = new LatLng(currentLocation.getLongitude(), currentLocation.getLatitude());

                        // update driver location
                        databaseReference.child(AppConstants.DB_EMPLOYEES_CHILD).child(driverId)
                                .child(AppConstants.DB_EMPLOYEES_LOCATION_CHILD)
                                .setValue(new EmployeeLocation(currentLatLng.latitude, currentLatLng.longitude));
                    }

                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(currentLatLng, 18);
                    mMap.animateCamera(cameraUpdate);

                }
            }
        }); // end mFusedLocationClient

    } // end onViewCreated




    private void getPendingOrderObject() {

        valueEventListener = databaseReference.child(AppConstants.DB_ORDERS_CHILD).child(orderId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getChildrenCount() >= 13) { // first time, it = 0, then 13 (complete child)
                            if(pendingOrder == null) {
                                if(null != dataSnapshot.getValue(Orders.class)) {
                                    pendingOrder = dataSnapshot.getValue(Orders.class);
                                }
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });

    }





    @Override
    public void onMapReady(GoogleMap googleMap) {
        if(mMap == null)
            mMap = googleMap;

        mMap.clear();
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(true);


        if (getContext() != null && getActivity() != null && ActivityCompat.checkSelfPermission(getContext(), ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                    AppConstants.LOCATION_PERMISSION_CODE);
        } else if(mMap != null) {
            mMap.setMyLocationEnabled(true);
        }

        if (markerOptions == null) {
            markerOptions = new MarkerOptions();
        }

    } // end onMapReady






    @Override
    public void onStart() {
        super.onStart();

        emp_status = driverId + "_user_requested";

        childEventListener1 = databaseReference.child(AppConstants.DB_PRE_ORDERS_CHILD)
                .orderByChild(AppConstants.DB_PRE_ORDERS_EMP_STATUS_CHILD).equalTo(emp_status)
                .addChildEventListener(new ChildEventListener() {

                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        preOrders = dataSnapshot.getValue(PreOrders.class);

                        if (null != preOrders) {
                            orderId = preOrders.getId();
                            if (!isInvitation) {
                                isInvitation = true;
                                statusGroup.setVisibility(View.INVISIBLE);
                                listenForPreOrderDeletion();
                                showInvitationOrderDialog(getContext(), preOrders);
                            }
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) { }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) { }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) { }

                    @Override
                    public void onCancelled(DatabaseError databaseError) { }

                }); // end childEventListener1


    } // end onStart








    private void listenForPreOrderDeletion() {
        if(preOrders != null) {
            childEventListener2 = databaseReference.child(AppConstants.DB_PRE_ORDERS_CHILD).child(preOrders.getId())
                    .addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) { }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) { }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                            if(invitationDialog != null && invitationDialog.isShowing()) {
                                invitationDialog.hide();
                                invitationDialog.dismiss();
                            }
                            ifTherePendingOrder = "no";
                            preOrders = null;
                            orderId = "";
                            if(isInvitation) {
                                isInvitation = false;
                                actionButton.setVisibility(View.INVISIBLE);
                                statusGroup.setVisibility(View.VISIBLE);
                                if(getActivity() != null) { // stop notification mp3 file
                                    getActivity().stopService(new Intent(getActivity(), SoundService.class));
                                }
                                if(mMap != null) {
                                    mMap.clear();
                                }
                                showCloseOrderDialog(AppConstants.DB_STATUS_CHILD_CANCELLED);
                            }
                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) { }

                        @Override
                        public void onCancelled(DatabaseError databaseError) { }
                    });
        }
    }








    private void showAlertDialog(String text1, String text2) {
        if(getContext() == null)
            return;

        final AlertDialog.Builder progressDialog = new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme);

        TextView title = new TextView(getContext());
        title.setText(getResources().getString(R.string.request_service));
        title.setTextSize(20);
        title.setGravity(Gravity.CENTER_HORIZONTAL);
        title.setPadding(0, 40, 0, 0);
        title.setTextColor(getResources().getColor(R.color.color_main_purple));

        TextView messageText = new TextView(getContext());
        messageText.setText(Html.fromHtml("<br><br>" + text1 + "<br>" + text2 + "<br>" ));
        messageText.setTextSize(15);
        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
        messageText.setTextColor(getResources().getColor(R.color.color_magor_purple));

        progressDialog.setCustomTitle(title);
        progressDialog.setView(messageText);
        //progressDialog.setCancelable(false);
        progressDialog.setPositiveButton("حسنا",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                dialog.cancel();
            }
        });

        progressDialog.show();
    } // end showAlertDialog







    private void showCloseOrderDialog(String status) {
        if(getContext() == null)
            return;

        closeDialog = new Dialog(getContext());

        closeDialog.setContentView(R.layout.dialog_close_order_layout);
        //closeDialog.setCancelable(false);


        Window dialogWindow = closeDialog.getWindow();

        if(closeDialog.getWindow() != null && dialogWindow != null && getActivity() != null) {

            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setGravity(Gravity.START | Gravity.TOP);

            WindowManager manager = getActivity().getWindowManager();
            DisplayMetrics displaymetrics = new DisplayMetrics();
            manager.getDefaultDisplay().getMetrics(displaymetrics);

            layoutParams.x = 75; // The new position of the X coordinates
            layoutParams.y = 300; // The new position of the Y coordinates
            layoutParams.width = displaymetrics.widthPixels - 150; // Width (75 from left, 75 from right)
            layoutParams.alpha = 0.9f; // Transparency

            dialogWindow.setAttributes(layoutParams);
        }


        ImageView statusImage = closeDialog.findViewById(R.id.statusImage);
        TextView statusTitle = closeDialog.findViewById(R.id.statusTitle);
        Button closeDialogButton = closeDialog.findViewById(R.id.closeDialog);

        if(status.equalsIgnoreCase(AppConstants.DB_STATUS_CHILD_COMPLETED)) {
            statusImage.setImageDrawable(getResources().getDrawable(R.drawable.success_message));
            statusTitle.setText(getResources().getString(R.string.order_completed_title));
            statusTitle.setTextColor(getResources().getColor(R.color.color_main_purple));
        } else if(status.equalsIgnoreCase(AppConstants.DB_STATUS_CHILD_CANCELLED)) {
            statusImage.setImageDrawable(getResources().getDrawable(R.drawable.error_message));
            statusTitle.setText(getResources().getString(R.string.cancel_order_from_user));
            statusTitle.setTextColor(getResources().getColor(R.color.color_red));
        }


        closeDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(closeDialog.isShowing()) {
                    closeDialog.hide();
                    closeDialog.dismiss();
                }
            }
        });

        closeDialog.show();
    } // end showCloseOrderDialog







    private void showInvitationOrderDialog(Context context, final PreOrders preOrders) {
        if(invitationDialog != null && invitationDialog.isShowing()) {
            invitationDialog.hide();
            invitationDialog.dismiss();
        }

        if(preOrders.getUserModel() == null)
            return;

        if(getActivity() != null) {
            getActivity().startService(new Intent(getActivity(), SoundService.class));
        }

        invitationDialog = new Dialog(context);

        invitationDialog.setContentView(R.layout.dialog_invitation_order_layout);
        //invitationDialog.setCancelable(false);


        Window dialogWindow = invitationDialog.getWindow();

        if(invitationDialog.getWindow() != null && dialogWindow != null && getActivity() != null) {

            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setGravity(Gravity.START | Gravity.TOP);

            WindowManager manager = getActivity().getWindowManager();
            DisplayMetrics displaymetrics = new DisplayMetrics();
            manager.getDefaultDisplay().getMetrics(displaymetrics);

            layoutParams.x = 75; // The new position of the X coordinates
            layoutParams.y = 100; // The new position of the Y coordinates
            layoutParams.width = displaymetrics.widthPixels - 150; // Width (75 from left, 75 from right)
            layoutParams.height = displaymetrics.heightPixels - 400; // Height (100 from top, 300 from bottom)
            layoutParams.alpha = 0.9f; // Transparency

            dialogWindow.setAttributes(layoutParams);

            invitationDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        }


        final CircularImageView userImage = invitationDialog.findViewById(R.id.userImage);

        TextView userName = invitationDialog.findViewById(R.id.userName);
        TextView sourceLocation = invitationDialog.findViewById(R.id.sourceLocation);
        ImageView orderDetails = invitationDialog.findViewById(R.id.orderDetails);

        Button contactButton = invitationDialog.findViewById(R.id.contactButton);


        userName.setText(preOrders.getUserModel().getName());
        sourceLocation.setText(preOrders.getSourceLocation().getName());


        // User image
        if(!preOrders.getUserModel().getAvatar().equalsIgnoreCase(AppConstants.DB_AVATAR_CHILD_DEFAULT) &&
                !preOrders.getUserModel().getAvatar().isEmpty()) {
            Picasso.with(getContext())
                    .load(preOrders.getUserModel().getAvatar())
                    .placeholder(R.drawable.profile_user)
                    .error(R.drawable.logo_orange)
                    .into(userImage);
        }


        contactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // open telephone application and contact driver ..

                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + preOrders.getUserModel().getMobile_no()));
                    startActivity(intent);
                } catch (Exception ex) {
                    ExceptionHandling.exceptionOccurred("MapsFragment", "Exception: " + ex.getMessage());
                }
            }
        });


        orderDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(invitationDialog != null && invitationDialog.isShowing()) {
                    invitationDialog.hide();
                    invitationDialog.dismiss();

                    // stop notification mp3 file
                    if(getActivity() != null) {
                        getActivity().stopService(new Intent(getActivity(), SoundService.class));
                    }
                    showInvitationOrderOnMap(preOrders);
                }
            }
        });

        invitationDialog.show();
    } // end showInvitationOrderDialog





    private void showPendingOrderOnMap(Orders orders) {
        if(mMap == null)
            return;

        mMap.clear();

        OrderLocation orderLocation;
        LatLng sourceLatLng, destinationLatLng;

        // Add the marker to the map
        IconGenerator iconGenerator = new IconGenerator(getContext());


        orderLocation = orders.getSourceLocation();
        sourceLatLng = new LatLng(orderLocation.getLatitude(), orderLocation.getLongitude());
        iconGenerator.setColor(Color.GREEN);

        mMap.addMarker(markerOptions
                .position(sourceLatLng)
                .icon(BitmapDescriptorFactory.fromBitmap(
                        iconGenerator.makeIcon(getString(R.string.transfer_from))))
                .anchor(iconGenerator.getAnchorU(), iconGenerator.getAnchorV()));


        orderLocation = orders.getDestinationLocation();
        destinationLatLng = new LatLng(orderLocation.getLatitude(), orderLocation.getLongitude());
        iconGenerator.setColor(Color.RED);

        mMap.addMarker(markerOptions
                .position(destinationLatLng)
                .icon(BitmapDescriptorFactory.fromBitmap(
                        iconGenerator.makeIcon(getString(R.string.transfer_to))))
                .anchor(iconGenerator.getAnchorU(), iconGenerator.getAnchorV()));


        iconGenerator.setColor(Color.BLACK);
        mMap.addMarker(markerOptions
                .position(currentLatLng)
                .icon(BitmapDescriptorFactory.fromBitmap(
                        iconGenerator.makeIcon(getString(R.string.my_location))))
                .anchor(iconGenerator.getAnchorU(), iconGenerator.getAnchorV()));


        String url = AppFunctions.getRequestURL(currentLatLng, sourceLatLng, destinationLatLng);
        TaskRequestDirections taskRequestDirections = new TaskRequestDirections();
        taskRequestDirections.execute(url);

        actionButtonText = getResources().getString(R.string.actionButtonAccept);
        actionButton.setImageBitmap(textAsBitmap(actionButtonText));
        actionButton.setVisibility(View.VISIBLE);

    } // end showPendingOrderOnMap






    private void showInvitationOrderOnMap(PreOrders preOrders) {
        if(mMap == null)
            return;

        mMap.clear();

        OrderLocation orderLocation;
        LatLng sourceLatLng, destinationLatLng;

        // Add the marker to the map
        IconGenerator iconGenerator = new IconGenerator(getContext());


        orderLocation = preOrders.getSourceLocation();
        sourceLatLng = new LatLng(orderLocation.getLatitude(), orderLocation.getLongitude());
        iconGenerator.setColor(Color.GREEN);

        mMap.addMarker(markerOptions
                .position(sourceLatLng)
                .icon(BitmapDescriptorFactory.fromBitmap(
                        iconGenerator.makeIcon(getString(R.string.transfer_from))))
                .anchor(iconGenerator.getAnchorU(), iconGenerator.getAnchorV()));


        orderLocation = preOrders.getDestinationLocation();
        destinationLatLng = new LatLng(orderLocation.getLatitude(), orderLocation.getLongitude());
        iconGenerator.setColor(Color.RED);

        mMap.addMarker(markerOptions
                .position(destinationLatLng)
                .icon(BitmapDescriptorFactory.fromBitmap(
                        iconGenerator.makeIcon(getString(R.string.transfer_to))))
                .anchor(iconGenerator.getAnchorU(), iconGenerator.getAnchorV()));


        iconGenerator.setColor(Color.BLACK);
        mMap.addMarker(markerOptions
                .position(currentLatLng)
                .icon(BitmapDescriptorFactory.fromBitmap(
                        iconGenerator.makeIcon(getString(R.string.my_location))))
                .anchor(iconGenerator.getAnchorU(), iconGenerator.getAnchorV()));


        String url = AppFunctions.getRequestURL(currentLatLng, sourceLatLng, destinationLatLng);
        TaskRequestDirections taskRequestDirections = new TaskRequestDirections();
        taskRequestDirections.execute(url);

        actionButtonText = getResources().getString(R.string.actionButtonAccept);
        actionButton.setImageBitmap(textAsBitmap(actionButtonText));
        actionButton.setVisibility(View.VISIBLE);

    } // end showInvitationOrderOnMap





    private Bitmap textAsBitmap(String text) {
        Paint paint = new Paint(ANTI_ALIAS_FLAG);

        if(text.equals(getResources().getString(R.string.actionButtonAccept))) {
            actionButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.action_button_accept)));
        } else if(text.equals(getResources().getString(R.string.actionButtonArrival))) {
            actionButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.action_button_arrival)));
        } else if(text.equals(getResources().getString(R.string.actionButtonReceipt))) {
            actionButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.action_button_receipt)));
        } else if(text.equals(getResources().getString(R.string.actionButtonFinish))) {
            actionButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.action_button_finish)));
        }

        paint.setTextSize(24);
        paint.setColor(Color.WHITE);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.0f); // round
        int height = (int) (baseline + paint.descent() + 0.0f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }




    private void showTrackOrderDialog(Context context, final User user) {
        if(user == null)
            return;

        driverDialog = new Dialog(context);

        driverDialog.setContentView(R.layout.dialog_track_order_layout);
        //driverDialog.setCancelable(false);


        Window dialogWindow = driverDialog.getWindow();

        if(driverDialog.getWindow() != null && dialogWindow != null && getActivity() != null) {

            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            dialogWindow.setGravity(Gravity.START | Gravity.TOP);

            WindowManager manager = getActivity().getWindowManager();
            DisplayMetrics displaymetrics = new DisplayMetrics();
            manager.getDefaultDisplay().getMetrics(displaymetrics);

            layoutParams.x = 75; // The new position of the X coordinates
            layoutParams.y = 200; // The new position of the Y coordinates
            layoutParams.width = displaymetrics.widthPixels - 150; // Width (75 from left, 75 from right)
            layoutParams.height = displaymetrics.heightPixels - 500; // Height (200 from top, 300 from bottom)
            layoutParams.alpha = 0.9f; // Transparency

            dialogWindow.setAttributes(layoutParams);

            driverDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        }


        final CircularImageView userImage = driverDialog.findViewById(R.id.userImage);

        TextView userName = driverDialog.findViewById(R.id.userName);
        ImageView orderDetails = driverDialog.findViewById(R.id.orderDetails);

        Button contactButton = driverDialog.findViewById(R.id.contactButton);

        userName.setText(user.getName());


        // Drive image
        if(!user.getAvatar().equalsIgnoreCase(AppConstants.DB_AVATAR_CHILD_DEFAULT) && !user.getAvatar().isEmpty()) {
            Picasso.with(getContext())
                    .load(user.getAvatar())
                    .placeholder(R.drawable.profile_user)
                    .error(R.drawable.logo_orange)
                    .into(userImage);
        }


        contactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // open telephone application and contact driver ..

                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + user.getMobile_no()));
                    startActivity(intent);
                } catch (Exception ex) {
                    ExceptionHandling.exceptionOccurred("MapsFragment", "Exception: " + ex.getMessage());
                }
            }
        });


        orderDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(driverDialog.isShowing()) {
                    driverDialog.hide();
                    //driverDialog.dismiss();
                }

                if(pendingOrder != null) {
                    ordersListener.onOrderSelected(pendingOrder, AppConstants.ORDERS_TO_DETAILS);
                }
            }
        });

        driverDialog.show();
    } // end showTrackOrderDialog









    @Override
    public void onResume() { super.onResume(); mMapView.onResume(); }

    @Override
    public void onPause() { super.onPause(); mMapView.onPause(); }

    @Override
    public void onStop() { super.onStop(); mMapView.onStop(); }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();

        // stop notification mp3 file
        if(getActivity() != null) {
            getActivity().stopService(new Intent(getActivity(), SoundService.class));
        }

        if(!emp_status.isEmpty()) {
            databaseReference.child(AppConstants.DB_PRE_ORDERS_CHILD)
                    .orderByChild(AppConstants.DB_PRE_ORDERS_EMP_STATUS_CHILD).equalTo(emp_status)
                    .removeEventListener(childEventListener1);
        }

        if(preOrders != null) {
            databaseReference.child(AppConstants.DB_PRE_ORDERS_CHILD).child(preOrders.getId())
                    .removeEventListener(childEventListener2);
        }

        if(!orderId.isEmpty()) {
            databaseReference.child(AppConstants.DB_ORDERS_CHILD).child(orderId)
                    .removeEventListener(valueEventListener);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onLowMemory() { super.onLowMemory(); mMapView.onLowMemory(); }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case AppConstants.LOCATION_PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (getContext() != null && getActivity() != null && ActivityCompat.checkSelfPermission(getContext(), ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED &&
                            ActivityCompat.checkSelfPermission(getContext(), ACCESS_COARSE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                                AppConstants.LOCATION_PERMISSION_CODE);
                    }
                    mMap.setMyLocationEnabled(true);
                }
                break;
        }
    }






    private String getRequestDirection(String requestURL) throws IOException {
        String responseString = "";

        InputStream inputStream = null;
        HttpURLConnection httpURLConnection = null;

        try {
            URL url = new URL(requestURL);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.connect();

            // Get the response result
            inputStream = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            StringBuilder stringBuilder = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }

            responseString = stringBuilder.toString();
            bufferedReader.close();
            inputStreamReader.close();
        } catch (MalformedURLException e) {
            ExceptionHandling.exceptionOccurred("MapsFragment", "MalformedURLException " + e.getMessage());
        } catch (IOException e) {
            ExceptionHandling.exceptionOccurred("MapsFragment", "IOException " + e.getMessage());
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if(httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }

        return  responseString;
    } // end getRequestDirection




    @SuppressLint("StaticFieldLeak")
    public class TaskRequestDirections extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {


            try {
                responseString = getRequestDirection(strings[0]);
            } catch (IOException e) {
                ExceptionHandling.exceptionOccurred("TaskRequestDirections", "IOException " + e.getMessage());
                e.printStackTrace();
            }

            return responseString;
        }


        @Override
        protected void onPostExecute(String response_string) {
            super.onPostExecute(response_string);
            responseString = response_string;

            // Parse JSON here
            TaskParser taskParser = new TaskParser();
            taskParser.execute(response_string);
        }


    } // End TaskRequestDirections class



    @SuppressLint("StaticFieldLeak")
    public class TaskParser extends AsyncTask<String, Void, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
            JSONObject jsonObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jsonObject = new JSONObject(strings[0]);

                DirectionsParser directionsParser = new DirectionsParser();
                routes = directionsParser.parse(jsonObject);
            } catch (JSONException e) {
                ExceptionHandling.exceptionOccurred("TaskParser", "JSONException " + e.getMessage());
            }

            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> lists) {
            // Get list route and disable it into the map

            ArrayList<LatLng> points;
            PolylineOptions polylineOptions = null;
            double lat, lon;

            for (List<HashMap<String, String>> path : lists) {
                points = new ArrayList<>();
                polylineOptions = new PolylineOptions();

                for (HashMap<String, String> point : path) {
                    lat = Double.parseDouble(point.get("lat"));
                    lon = Double.parseDouble(point.get("lon"));

                    points.add(new LatLng(lat, lon));
                } // inner for
                polylineOptions.addAll(points);
                polylineOptions.width(18);
                polylineOptions.color(getResources().getColor(R.color.color_main_purple));
                polylineOptions.geodesic(true);
            } // outer for

            if (polylineOptions != null) {
                mMap.addPolyline(polylineOptions);
            }// else {
            //Toast.makeText(getContext(), "Direction not found!", Toast.LENGTH_LONG).show();
            //}
        }

    } // End Task Parser class





}
