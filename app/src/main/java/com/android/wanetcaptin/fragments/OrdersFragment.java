package com.android.wanetcaptin.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.wanetcaptin.R;

import java.util.ArrayList;
import java.util.List;


public class OrdersFragment extends Fragment {


    public OrdersFragment() { }

    public static OrdersFragment newInstance() {
        return new OrdersFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_orders, container, false);
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ViewPager ordersViewPager = view.findViewById(R.id.ordersViewPager);
        TabLayout ordersTabs = view.findViewById(R.id.ordersTabs);

        Adapter adapter = new Adapter(getChildFragmentManager());
        adapter.addFragment(CurrentOrdersFragment.newInstance(), getResources().getString(R.string.tab_current));
        adapter.addFragment(PreviousOrdersFragment.newInstance(), getResources().getString(R.string.tab_previous));

        ordersViewPager.setAdapter(adapter);

        ordersTabs.setupWithViewPager(ordersViewPager);
    }


    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    } // end Adapter class


}




