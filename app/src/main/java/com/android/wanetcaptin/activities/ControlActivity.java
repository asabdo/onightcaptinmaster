package com.android.wanetcaptin.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.wanetcaptin.models.ExceptionHandling;
import com.android.wanetcaptin.models.AppConstants;


import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;


public class ControlActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // ToDo -For test
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                ) {

            ActivityCompat.requestPermissions(this, new String[]
                            {ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                    AppConstants.LOCATION_PERMISSION_CODE);

        } else {

            Intent intent;

            // check if first run or not ..
            SharedPreferences sharedPreferences = getSharedPreferences(AppConstants.FILE_NAME, MODE_PRIVATE);

            if (sharedPreferences != null) {

                if (sharedPreferences.getString(AppConstants.SIGN_IN_KEY, AppConstants.DEFAULT_VALUE)
                        .equalsIgnoreCase("yes")) {
                    // Sign in ..

                    if (sharedPreferences.getString(AppConstants.FIRST_TIME_KEY, AppConstants.DEFAULT_VALUE)
                            .equalsIgnoreCase("no")) {
                        // Not first time .. so ==> Go to Main Activity
                        intent = new Intent(ControlActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();

                    } else {
                        // first time .. so ==> Go to TermsOfUse Activity ..
                        intent = new Intent(ControlActivity.this, TermsOfUseActivity.class);
                        startActivity(intent);
                        finish();
                    }

                } else {

                    // Not signed in .. so ==> Go to ViewPagerActivity => SignInActivity
                    intent = new Intent(ControlActivity.this, ViewPagerActivity.class);
                    startActivity(intent);
                    finish();

                }

            }

        }

    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case AppConstants.LOCATION_PERMISSION_CODE:
                // ToDo remove this before lunch
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
                break;
        }
    }

}
