package com.android.wanetcaptin.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.wanetcaptin.models.AppConstants;

import com.android.wanetcaptin.models.AppFunctions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.android.wanetcaptin.R;


public class SignInActivity extends AppCompatActivity {


    EditText numberAccount, passAccount;
    Button loginAccount;

    private FirebaseAuth mAuth;

    private String driverId;
    private String phoneNumber = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_sign_in);

        numberAccount = findViewById(R.id.numberAccount);
        passAccount = findViewById(R.id.passAccount);
        loginAccount = findViewById(R.id.loginAccount);

        mAuth = FirebaseAuth.getInstance();


        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }


        AppFunctions appFunctions = new AppFunctions(getApplicationContext());
        final boolean isOnline = appFunctions.isOnline();

        loginAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!isOnline) {
                    showAlertDialog(getResources().getString(R.string.no_internet_connection),
                            getResources().getString(R.string.no_internet_connection_description));
                } else if (numberAccount.getText().toString().trim().isEmpty()) {
                    numberAccount.setError(getResources().getString(R.string.error_enter_account_number));
                } else if (passAccount.getText().toString().trim().isEmpty()) {
                    passAccount.setError(getResources().getString(R.string.error_enter_account_password));
                } else {
                    confirmDialog();
                }

                // Hide the keyboard ..
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(passAccount.getWindowToken(), 0);
                }

            }
        }); // end loginAccount listener ..



    } // end onCreate function





    private void confirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);

        // for test : +97 ToDo edit it to +966

        phoneNumber = numberAccount.getText().toString().trim();
        //phoneNumber = "+970" + numberAccount.getText().toString().trim();

        if (TextUtils.isEmpty(phoneNumber)) {
            numberAccount.setError(getResources().getString(R.string.error_enter_phone_number));
        } else if(phoneNumber.length() < 10) {
            numberAccount.setError(getResources().getString(R.string.error_incorrect_phone_number));
        }

        if(phoneNumber.startsWith("00"))
            phoneNumber = phoneNumber.replaceFirst("00", "+"); // replace 00 by + in the first if exist ..
        else if(phoneNumber.startsWith("0")) {
            phoneNumber = phoneNumber.substring(1);
            phoneNumber = "+966" + phoneNumber;
        } else if(phoneNumber.startsWith("+970") || phoneNumber.startsWith("970")) { // handle dummy number for test
            phoneNumber = phoneNumber.replace("+", "");
            phoneNumber = phoneNumber.replaceFirst("970", "+970");
        }

        TextView title = new TextView(this);
        title.setText("الرجاء التأكيد");
        title.setTextSize(20);
        title.setGravity(Gravity.CENTER_HORIZONTAL);
        title.setPadding(0, 40, 0, 0);
        title.setTextColor(getResources().getColor(R.color.color_red));

        TextView messageText = new TextView(this);
        messageText.setText(Html.fromHtml("<br>"
                + "هل الرقم التالي هو رقمك"
                + "<br> <br>"
                + phoneNumber));
        messageText.setTextSize(17);
        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
        messageText.setTextColor(getResources().getColor(R.color.color_main_purple));


        builder
                .setCustomTitle(title)
                .setView(messageText)
                .setCancelable(false)
                .setPositiveButton("نعم", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        checkLoginInformation();
                    }
                })
                .setNegativeButton("لا", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })

                .show();
    } // end confirmDialog



    private void showAlertDialog(String text1, String text2) {
        final AlertDialog.Builder progressDialog = new AlertDialog.Builder(getApplicationContext(), R.style.AlertDialogTheme);

        TextView title = new TextView(getApplicationContext());
        title.setText(getResources().getString(R.string.request_service));
        title.setTextSize(20);
        title.setGravity(Gravity.CENTER_HORIZONTAL);
        title.setPadding(0, 40, 0, 0);
        title.setTextColor(getResources().getColor(R.color.color_main_purple));

        TextView messageText = new TextView(getApplicationContext());
        messageText.setText(Html.fromHtml("<br><br>" + text1 + "<br>" + text2 + "<br>" ));
        messageText.setTextSize(15);
        messageText.setGravity(Gravity.CENTER_HORIZONTAL);
        messageText.setTextColor(getResources().getColor(R.color.color_magor_purple));

        progressDialog.setCustomTitle(title);
        progressDialog.setView(messageText);
        //progressDialog.setCancelable(false);
        progressDialog.setPositiveButton("حسنا",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                dialog.cancel();
            }
        });

        progressDialog.show();
    } // end showAlertDialog()



    private void checkLoginInformation () {

        final ProgressDialog verificationProgressDialog = new ProgressDialog(this, R.style.AlertDialogTheme);

        TextView title = new TextView(this);
        title.setText(Html.fromHtml("<br>" +
                getResources().getString(R.string.check_login_information_title)
                + "<br><br>" +
                getResources().getString(R.string.check_login_information_description) ));

        title.setTextSize(17);
        title.setGravity(Gravity.CENTER_HORIZONTAL);
        title.setPadding(0, 40, 0, 0);
        title.setTextColor(getResources().getColor(R.color.color_main_purple));

        verificationProgressDialog.setCustomTitle(title);
        verificationProgressDialog.setCancelable(false);

        verificationProgressDialog.show();


        final String email = phoneNumber + "@oneight.sa";
        final String pass = passAccount.getText().toString().trim();

        // check database ..
        mAuth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            FirebaseUser driver = task.getResult().getUser();
                            driverId = driver.getUid();

                            SharedPreferences sharedPreferences = getSharedPreferences(AppConstants.FILE_NAME, MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(AppConstants.SIGN_IN_KEY, "yes");
                            editor.putString(AppConstants.DB_ID_CHILD, driverId);
                            editor.putString(AppConstants.DB_MOBILE_NO_CHILD, phoneNumber);
                            editor.apply();

                            verificationProgressDialog.hide();
                            verificationProgressDialog.dismiss();

                            startActivity(new Intent(SignInActivity.this, ControlActivity.class));

                        } else {

                            verificationProgressDialog.hide();
                            verificationProgressDialog.dismiss();

                            Toast.makeText(getApplicationContext(),
                                    getResources().getString(R.string.error_verification_failed),
                                    Toast.LENGTH_SHORT).show();

                        }
                    }
                });

    } // end check login information ..




    @Override
    protected void onPause() {
        super.onPause();
        // Hide the keyboard before go to Main activity ..
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
        }
    }




}

