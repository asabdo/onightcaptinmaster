package com.android.wanetcaptin.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.wanetcaptin.R;
import com.android.wanetcaptin.fragments.OrdersTimeLineFragment;
import com.android.wanetcaptin.fragments.MapsFragment;
import com.android.wanetcaptin.fragments.OrdersFragment;
import com.android.wanetcaptin.fragments.OrdersDetailsFragment;
import com.android.wanetcaptin.fragments.ProfileFragment;
import com.android.wanetcaptin.fragments.TermsOfUseFragment;
import com.android.wanetcaptin.models.Employee;
import com.android.wanetcaptin.models.ExceptionHandling;
import com.android.wanetcaptin.models.AppConstants;
import com.android.wanetcaptin.models.LoadFragmentListener;
import com.android.wanetcaptin.models.Orders;
import com.android.wanetcaptin.models.OrdersListener;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OrdersListener, LoadFragmentListener {


    NavigationView navigationView;
    LinearLayout profileLayout;
    CircularImageView profileImage;
    TextView profileName, profileMobileNumber;
    DrawerLayout drawer;

    String driverAvatar = "", currentAvatar = "";
    private boolean ifFirstTime = true;

    private SharedPreferences sharedPreferences = null;
    private SharedPreferences.Editor editor = null;

    private DatabaseReference databaseReference = null;
    private ValueEventListener valueEventListener = null;

    private MapsFragment mapsFragment = null;
    private OrdersFragment ordersFragment = null;
    private TermsOfUseFragment termsOfUseFragment = null;
    private ProfileFragment profileFragment = null;
    private OrdersDetailsFragment ordersDetailsFragment = null;
    private OrdersTimeLineFragment ordersTimeLineFragment = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);


        View headerView = navigationView.getHeaderView(0);

        profileLayout = headerView.findViewById(R.id.profileLayout);
        profileImage = headerView.findViewById(R.id.profileImage);
        profileName = headerView.findViewById(R.id.profileName);
        profileMobileNumber = headerView.findViewById(R.id.profilePhoneNumber);
        TextView menuLogOut = findViewById(R.id.menuLogOut);


        sharedPreferences = getSharedPreferences(AppConstants.FILE_NAME, MODE_PRIVATE);

        if(sharedPreferences != null) {
            String driverMobileNo = sharedPreferences.getString(AppConstants.DB_MOBILE_NO_CHILD, AppConstants.DEFAULT_VALUE);
            if (!driverMobileNo.equalsIgnoreCase(AppConstants.DEFAULT_VALUE))
                profileMobileNumber.setText(driverMobileNo);
        }


        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if(currentUser != null) {
            databaseReference = FirebaseDatabase.getInstance().getReference(AppConstants.DB_EMPLOYEES_CHILD)
                    .child(currentUser.getUid());
        }


        profileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(profileFragment, "ProfileFragment");
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        menuLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sharedPreferences != null) {
                    editor = sharedPreferences.edit();
                    editor.putString(AppConstants.SIGN_IN_KEY, "no");
                    editor.apply();
                }

                FirebaseAuth.getInstance().signOut();

                startActivity(new Intent(MainActivity.this, SignInActivity.class));
            }
        });


        mapsFragment = MapsFragment.newInstance();
        ordersFragment = OrdersFragment.newInstance();
        termsOfUseFragment = TermsOfUseFragment.newInstance();
        profileFragment = ProfileFragment.newInstance();
        ordersDetailsFragment = OrdersDetailsFragment.newInstance();
        ordersTimeLineFragment = OrdersTimeLineFragment.newInstance();

        loadFragment(mapsFragment, "MapsFragment");
        loadFragment(ordersFragment, "OrdersFragment");
        loadFragment(termsOfUseFragment, "TermsOfUseFragment");
        loadFragment(profileFragment, "ProfileFragment");
        loadFragment(ordersDetailsFragment, "OrdersDetailsFragment");
        loadFragment(ordersTimeLineFragment, "OrdersTimeLineFragment");

    }


    @Override
    protected void onStart() {
        super.onStart();

        if(ifFirstTime) {
            loadFragment(mapsFragment, "MapsFragment");
            ifFirstTime = false;
        }

        updateProfileImage();

    } // end onStart




    private void updateProfileImage() {

        valueEventListener = databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Employee employee = dataSnapshot.getValue(Employee.class);

                if(employee != null && employee.getAvatar() != null &&
                        !employee.getAvatar().equalsIgnoreCase(AppConstants.DB_AVATAR_CHILD_DEFAULT)) {

                    if(employee.getName() != null && employee.getEmail() != null && employee.getAvatar() != null && sharedPreferences != null) {
                        // If remove the app and reinstall it, check the shared ..
                        String name = sharedPreferences.getString(AppConstants.DB_NAME_CHILD, AppConstants.DEFAULT_VALUE);
                        String email = sharedPreferences.getString(AppConstants.DB_EMAIL_CHILD, AppConstants.DEFAULT_VALUE);
                        String avatar = sharedPreferences.getString(AppConstants.DB_AVATAR_CHILD,
                                AppConstants.DB_AVATAR_CHILD_DEFAULT);
                        String carModel = sharedPreferences.getString(AppConstants.DB_CAR_MODEL_CHILD, AppConstants.DEFAULT_VALUE);
                        String carNumber = sharedPreferences.getString(AppConstants.DB_CAR_NUMBER_CHILD,
                                AppConstants.DB_AVATAR_CHILD_DEFAULT);

                        editor = sharedPreferences.edit();

                        if(name.equalsIgnoreCase(AppConstants.DEFAULT_VALUE))
                            editor.putString(AppConstants.DB_NAME_CHILD, employee.getName());
                        if(email.equalsIgnoreCase(AppConstants.DEFAULT_VALUE))
                            editor.putString(AppConstants.DB_EMAIL_CHILD, employee.getEmail());
                        if(avatar.equalsIgnoreCase(AppConstants.DB_AVATAR_CHILD_DEFAULT))
                            editor.putString(AppConstants.DB_AVATAR_CHILD, employee.getAvatar());
                        if(carModel.equalsIgnoreCase(AppConstants.DEFAULT_VALUE))
                            editor.putString(AppConstants.DB_CAR_MODEL_CHILD, employee.getCar_model());
                        if(carNumber.equalsIgnoreCase(AppConstants.DB_AVATAR_CHILD_DEFAULT))
                            editor.putString(AppConstants.DB_CAR_NUMBER_CHILD, employee.getCar_no());

                        editor.apply();

                        currentAvatar = employee.getAvatar();
                        if(!currentAvatar.equalsIgnoreCase(driverAvatar)) {

                            driverAvatar = currentAvatar;
                            Picasso.with(getApplicationContext())
                                    .load(currentAvatar)
                                    .placeholder(R.drawable.profile_user)
                                    .error(R.drawable.logo_orange)
                                    .into(profileImage);
                        }

                    }
                }
            } // en onDataChange

            @Override
            public void onCancelled(DatabaseError databaseError) { }

        });

    } // end update profile image



    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (getFragmentManager().getBackStackEntryCount() > 1) {
                getFragmentManager().popBackStack();
            }

            /*
            int fragments = getSupportFragmentManager().getBackStackEntryCount();

            if (fragments == 1) {
                finish();
            }
            else {
                if (getFragmentManager().getBackStackEntryCount() > 1) {
                    getFragmentManager().popBackStack();
                } else {
                    super.onBackPressed();
                }
            }
            */
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }




    @Override
    protected void onDestroy() {
        super.onDestroy();

        databaseReference.child(AppConstants.DB_STATUS_CHILD).setValue(AppConstants.DB_STATUS_CHILD_UNAVAILABLE);

        databaseReference.removeEventListener(valueEventListener);
    }




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.menu_main_maps) {
            updateProfileImage();
            loadFragment(mapsFragment, "MapsFragment");
        } else if (id == R.id.menu_my_orders) {
            loadFragment(ordersFragment, "OrdersFragment");
        } else if (id == R.id.menu_terms_of_use) {
            loadFragment(termsOfUseFragment, "TermsOfUseFragment");
        } else if (id == R.id.menu_log_out) {
            // not used, it is not visible
            if(sharedPreferences != null) {
                editor = sharedPreferences.edit();
                editor.putString(AppConstants.SIGN_IN_KEY, "no");
                editor.apply();
            }

            FirebaseAuth.getInstance().signOut();

            startActivity(new Intent(MainActivity.this, SignInActivity.class));
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }




    @Override
    public void onOrderSelected(Orders orders, String source) {

        if(source.equalsIgnoreCase(AppConstants.ORDERS_TO_DETAILS)) {
            ordersDetailsFragment.setOrders(orders);
            loadFragment(ordersDetailsFragment, "OrdersDetailsFragment");
        } else if(source.equalsIgnoreCase(AppConstants.DETAILS_TO_TIME_LINE)) {
            ordersTimeLineFragment.setOrders(orders);
            loadFragment(ordersTimeLineFragment, "OrdersTimeLineFragment");
        } else if(source.equalsIgnoreCase(AppConstants.TIME_LINE_TO_DETAILS)) {
            ordersDetailsFragment.setOrders(orders);
            loadFragment(ordersDetailsFragment, "OrdersDetailsFragment");
        }
    }



    @Override
    public void loadFragment(String fragmentName) {
        if(fragmentName.equalsIgnoreCase("MapsFragment")) {
            loadFragment(mapsFragment, "MapsFragment");
        } else if(fragmentName.equalsIgnoreCase("OrdersFragment")) {
            loadFragment(ordersFragment, "OrdersFragment");
        } else if(fragmentName.equalsIgnoreCase("ProfileFragment")) {
            loadFragment(profileFragment, "ProfileFragment");
        }
    }




    private void loadFragment (Fragment fragment, String fragmentName) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(fragmentName);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();


        if (!isFinishing() && !isDestroyed()) {
            if (currentFragment != null) {
                fragmentTransaction.show(fragment);
            } else {
                try {
                    fragmentTransaction.add(R.id.fragment_container, fragment, fragmentName);
                } catch (Exception ex) {
                    ExceptionHandling.exceptionOccurred("MainActivity", "Exception: " + ex.getMessage());
                }
            }

            switch (fragmentName) {
                case "MapsFragment":
                    fragmentTransaction.hide(ordersFragment);
                    fragmentTransaction.hide(termsOfUseFragment);
                    fragmentTransaction.hide(profileFragment);
                    fragmentTransaction.hide(ordersDetailsFragment);
                    fragmentTransaction.hide(ordersTimeLineFragment);
                    break;
                case "OrdersFragment":
                    fragmentTransaction.hide(mapsFragment);
                    fragmentTransaction.hide(termsOfUseFragment);
                    fragmentTransaction.hide(profileFragment);
                    fragmentTransaction.hide(ordersDetailsFragment);
                    fragmentTransaction.hide(ordersTimeLineFragment);
                    break;
                case "TermsOfUseFragment":
                    fragmentTransaction.hide(mapsFragment);
                    fragmentTransaction.hide(ordersFragment);
                    fragmentTransaction.hide(profileFragment);
                    fragmentTransaction.hide(ordersDetailsFragment);
                    fragmentTransaction.hide(ordersTimeLineFragment);
                    break;
                case "ProfileFragment":
                    fragmentTransaction.hide(mapsFragment);
                    fragmentTransaction.hide(ordersFragment);
                    fragmentTransaction.hide(termsOfUseFragment);
                    fragmentTransaction.hide(ordersDetailsFragment);
                    fragmentTransaction.hide(ordersTimeLineFragment);
                    break;
                case "OrdersDetailsFragment":
                    fragmentTransaction.hide(mapsFragment);
                    fragmentTransaction.hide(ordersFragment);
                    fragmentTransaction.hide(termsOfUseFragment);
                    fragmentTransaction.hide(profileFragment);
                    fragmentTransaction.hide(ordersTimeLineFragment);
                    break;
                case "OrdersTimeLineFragment":
                    fragmentTransaction.hide(mapsFragment);
                    fragmentTransaction.hide(ordersFragment);
                    fragmentTransaction.hide(termsOfUseFragment);
                    fragmentTransaction.hide(profileFragment);
                    fragmentTransaction.hide(ordersDetailsFragment);
                    break;
            }

            fragmentTransaction.addToBackStack(fragmentName).commit();

        }
    }


}
