package com.android.wanetcaptin.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.android.wanetcaptin.R;
import com.android.wanetcaptin.models.AppConstants;


public class TermsOfUseActivity extends AppCompatActivity {


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_terms_of_use);


        WebView termsContent = findViewById(R.id.termsContent);

        WebSettings webSetting = termsContent.getSettings();
        webSetting.setBuiltInZoomControls(true);
        webSetting.setJavaScriptEnabled(true);

        termsContent.setWebViewClient(new WebViewClient());
        termsContent.loadUrl("file:///android_asset/conditions.htm");


        Button acceptButton = findViewById(R.id.acceptButton);

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Change the Shared References ..
                SharedPreferences sharedPreferences = getSharedPreferences(AppConstants.FILE_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(AppConstants.FIRST_TIME_KEY, "no");
                editor.apply();

                // Go to Main Activity ..
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });



    }
}
