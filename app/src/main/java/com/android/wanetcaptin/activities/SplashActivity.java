package com.android.wanetcaptin.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.wanetcaptin.R;
import com.android.wanetcaptin.models.ExceptionHandling;


public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_splash);

        int secondsDelayed = 3;

        try {

            new Handler().postDelayed(new Runnable() {
                public void run() {
                    // Go to View Pager Activity
                    Intent intent = new Intent(SplashActivity.this, ControlActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, secondsDelayed * 1000);
        } catch (Exception e) {
            ExceptionHandling.exceptionOccurred("Splash Activity", "Exception " + e.getMessage());
        }

    }

}
